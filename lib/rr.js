;(function (global, undefined) {
'use strict';

var array_proto = [];
var slice = array_proto.slice;
var object_proto = {};
var to_str = object_proto.toString;
var native_timeout = setTimeout;
var native_interval = setInterval;


/**
 * A low-level bind implementation optimized for the most basic usage. Simply
 * binds a function to a context, without any currying or type checking.
 * 
 * @param  {Function} fn      the function to bind the context to
 * @param  {[type]}   context the object to be bound
 * @return {Function}         the bound function
 */
function bind (fn, context) {
	return function () {
		return fn.call(context, arguments);
	}
}

function partial (fn) {
	var args = slice.call(arguments, 1);
	return function () {
		return fn.apply(this, args.concat(slice.call(arguments)));
	}
}

// returns a function that can only be called once during a specified interval
function throttle (fn, interval) {
	var last; // the last time the function was executed
	return function () {
		var context = this, args = arguments;
		var now = new Date;
		// execute if not previously executed or if enough time has elapsed
		if (!last || now - last >= interval) {
			last = now;
			return fn.apply(context, args);
		}
	};
}

// returns a new function that accepts the context the original function should
// be called in as the first argument
// example:
// 	var slice = unbind(Array.prototype.slice);
// 	slice([1,2,3], 1);
// 	=> [2,3]
function unbind (fn) {
	return function (context /* args... */) {
		return fn.apply(context, slice.call(arguments, 1));
	}
}

// returns a function that will only execute after it has stopped being
// called for a specified amount of time
function debounce (fn, delay) {
	var scheduled;
	return function () {
		var context = this, args = arguments;
		clearTimeout(scheduled); // cancel previously scheduled execution
		scheduled = setTimeout(function () {
			fn.apply(context, args);
		}, delay);
	};
}

var isArray = Array.isArray || function (thing) {
	return (thing && typeof thing == 'object') ? to_str.call(thing) == '[object Array]' : false;
};

var isFunction = typeof (/./) !== 'function' 
	? function (thing) {
		return typeof thing == 'function';
	}

	: function (thing) { // older versions of webkit need this
		return typeof thing == 'function' && to_string.call(thing) == '[object Function]';
	}
;

function has (obj, key) {
	return object_proto.hasOwnProperty.call(obj, key);
}

function getOwn (obj, key) {
	var own;
	if (has(obj, key)) {
		own = obj[key];
	}
	return own;
}

var hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString');
var dontEnums = [
	'toString',
	'toLocaleString',
	'valueOf',
	'hasOwnProperty',
	'isPrototypeOf',
	'propertyIsEnumerable',
	'constructor'
];
var dontEnumsLength = dontEnums.length;


var _keys = Object.keys || hasDontEnumBug ?

	// environment has the don't enum bug
	function (obj) {
		var result = [], prop, index = -1;

		for (prop in obj) {
			if (has(obj, prop)) {
				result.push(prop);
			}
		}

		// work around for enum bug
		while (++index < dontEnumsLength) {
			if (has(obj, dontEnums[index])) {
				result.push(prop);
			}
		}

		return result;
	}

	:

	// environment doesn't have the enum bug
	function (obj) {
		var result = [], prop;

		for (prop in obj) {
			if (has(obj, prop)) {
				result.push(prop);
			}
		}

		return result;
	}
;

function keys (object) {
	if (object == null) return [];
	return _keys(object);
}

function each (collection, iterator, context) {
	if (collection == null) return collection;

	var length = collection.length;
	var index = -1;
	var key;
	var collection_keys;

	if (length === +length) {
		// treat the collection like an array
		while (++index < length) {
			// call the iterator, and break if it returns false
			if (iterator.call(context, collection[index], index, collection) === false) {
				break;
			}
		}
	}
	else {
		// treat the collection like an object
		collection_keys = keys(collection);
		length = collection_keys.length;

		while (++index < length) {
			key = collection_keys[index];
			// call the iterator, and break if it returns false
			if (iterator.call(context, collection[key], key, collection) === false) {
				break;
			}
		}
	}
}

function map (collection, iterator, context) {
	var results = [];
	var length = collection.length;
	var index = -1;

	if (length === +length) {
		while (++index < length) {
			results.push(iterator.call(context, collection[index], index, collection))
		}
	} else {
		each(collection, function (prop, key, collection) {
			results.push(iterator.call(context, prop, key, collection));
		}, context);
	}
	return results;
}

function reduce (collection, iterator, memo, context) {
	var init = arguments.length > 2;

	each(collection, function (val, key, list) {
		if (!init) {
			memo = val;
			init = true;
		} else {
			memo = iterator.call(context, memo, val, key, collection);
		}
	});

	return memo;
}

function filter (collection, iterator, context) {
	var length = collection.length;
	var index = -1;
	var result, collection_keys, key, val;

	if (length === +length) {
		result = [];
		while (++index < length) {
			if (iterator.call(context, collection[index], index, collection)) {
				result.push(collection[index]);
			}
		}
	} else {
		result = {};
		collection_keys = keys(collection);
		length = collection_keys.length;
		while (++index < length) {
			key = collection_keys[index];
			val = collection[key];
			if (iterator.call(context, val, key, collection)) {
				result[key] = val;
			}
		}
	}
	return result;
}

function invoke (collection, method) {
	var args = slice.call(arguments, 2);
	var fun = isFunction(method);

	return map(collection, function (val) {
		return (fun? method : val[method]).apply(val, args);
	});
}

function size (collection) {
	if (collection.length === +collection.length) {
		return collection.length;
	}
	return keys(collection).length;
}

function pluck (collection, property) {
	return map(collection, function (val) {
		return val[property];
	});
}

// gets the values of a collection as an array
// (bonus: will return a shallow clone of an array)
function values (collection) {
	var length = size(collection);
	var own_keys = keys(collection);
	var index = -1;
	var result = [];

	while (++index < length) {
		result.push(collection[own_keys[index]]);
	}
	return result;
}

// Fisher–Yates Shuffle
// this is cool: http://bost.ocks.org/mike/shuffle/
function shuffle (collection) {
	
	// copy the array (or the object's values)
	var result = values(collection);
	var length = result.length;
	var temp, rand;

	while (length) {
		// get a random int between 0 and the number of remaining unshuffled values
		rand = Math.floor(Math.random() * length--);
		
		// swap the last unshuffled value with a randomly selected unshuffled value
		temp = result[length];
		result[length] = result[rand];
		result[rand] = temp;
	}

	return result;
}

function mixin (object, source) {
	var own_keys = keys(source);
	var l = own_keys.length;
	var i = -1;
	var key;

	while (++i < l) {
		key = own_keys[i];
		if (!has(object, key)) {
			object[key] = source[key];
		}
	}

	return object;
}

function extend (object, source) {
	var own_keys = keys(source);
	var l = own_keys.length;
	var i = -1;
	var key;

	while (++i < l) {
		key = own_keys[i];
		object[key] = source[key];
	}

	// each(source, function (prop, key, source) {
	// 	object[key] = prop;
	// });
	return object;
}

// returns a new object, the result of a shallow merge of each supplied argument
function merge (/* objects */) {
	// TODO: should perform a deep copy?
	var merged = {};
	var l = arguments.length;
	var i = -1;

	while (++i < l) {
		extend(merged, arguments[i]);
	}

	return merged;
}

var _create = Object.create || (function () {
	function F(){}
	return function (prototype) {
		F.prototype = prototype;
		return new F();
	}
}());

function create (prototype, props) {
	var obj = _create(prototype);
	
	if (props) {
		each(props, function (prop, key) {
			obj[key] = prop;
		});
	}

	return obj;
}

function range (length, start, interval) {
	if (!start) start = 0;
	if (arguments.length < 3) interval = 1;
	var range = Array(length);
	var index = -1;

	while (++index < length) {
		range[index] = start + index * interval;
	}

	return range;
}

function namespace (context, path, props) {

	if (context == null) return;

	var levels = path.split('.');
	var l = levels.length;
	var i = -1;
	var namespace = context;

	while (++i < l) {
		if (namespace[levels[i]] == null) {
			namespace[levels[i]] = {};
		}

		namespace = namespace[levels[i]];
	}

	extend(namespace, props);
	return namespace;
}

var html_escaper = /[&<>"']/g;
var html_escaper_map = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#x27;'
};

function escapeHtml (string) {
	return string.replace(html_escaper, function (match) {
		return html_escaper_map[match];
	});
}

var regexp_escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g;
var regexp_escape_map = {
	"'":      "'",
	'\\':     '\\',
	'\r':     'r',
	'\n':     'n',
	'\t':     't',
	'\u2028': 'u2028',
	'\u2029': 'u2029'
};

function escapeRegExp (string) {
	return string.replace(regexp_escaper, function (match) {
		return '\\' + regexp_escape_map[match];
	});
}

// extremely basic templating function, takes a string, such as:
// 	var string = '<a href="{{url}}">{{text}}</a> {{ some_other_key }}';
// and a data object: 
// 	var data = { text: 'Home', url: 'index.html'};
// and returns a new string:
// 	'<a href="index.html">Home</a> ';
// notice some_other_key was replaced with an empty string
function compile (string, data) {
	var data_keys = keys(data);
	var l = data_keys.length;
	var i = -1;
	var key, reg;

	while (++i < l) {
		key = data_keys[i];
		reg = new RegExp('\\{\\{\\s\*' + escapeRegExp(key) + '\\s\*\\}\\}', 'g');
		string = string.replace(reg, data[key]);
	}
	// remove any unreplaced variables from the template before returning
	return string.replace(/\{\{\s*[^\}]+\s*\}\}/g, '');
}


function template (string) {
	// Simple JavaScript Templating
	// John Resig - http://ejohn.org/ - MIT Licensed
	// http://ejohn.org/blog/javascript-micro-templating/
	var source = "var p=[],print=function(){p.push.apply(p,arguments);};" +
		// Introduce the data as local variables using with(){}
		"with(obj){p.push('" +

		// Convert the template into pure JavaScript
		string
			.replace(/[\r\t\n]/g, " ")
			.split("<%").join("\t")
			.replace(/((^|%>)[^\t]*)'/g, "$1\r")
			.replace(/\t=(.*?)%>/g, "',$1,'")
			.split("\t").join("');")
			.split("%>").join("p.push('")
			.split("\r").join("\\'")
		+ "');}return p.join('');"
	;

	// compile the function
	var fn = new Function("obj", source);
	// provide access to the generated function body and original template source
	fn.body = source;
	fn.source = string;
	return fn;
}

function trim (string) {
	return string.replace(/^\s+|\s+$/g,'');
}

// a convenience method for binding a prototype and constructor together.
// if the prototype doesn't contain a constructor function, an empty one
// will be used.
function Prototype (prototype) {
	var constructor = prototype.constructor || new Function();
	constructor.prototype = prototype;
	return constructor;
}

var Mediator = Prototype({

	constructor: function () {
		this._channels = {};
	},

	// set or augment a channels options
	// available options:
	// 	(obj) 'context' the context in which callbacks should be executed
	// 	(int) 'times' the number of times an event is allowed to occur
	// 	(bool) 'expires' if true, callbacks added after an event has occurred 
	// 	                 the allowed number of times will be ignored, otherwise
	// 	                 they will be immediately executed with the same 
	// 	                 arguments as the final occurance
	// 	
	options: function (channel, options) {
		namespace(this._channels, channel + '.options', options);
	},

	subscribe: function (channel, callback) {
		var ns = namespace(this._channels, channel);
		var context = ns.options ? ns.options.context : null;
		if ('options' in ns && 'times' in ns.options && 'calls' in ns && ns.calls >= ns.options.times) {
			// event is no longer relevant, call immediately
			setTimeout(function () {
				callback.apply(context, ns.args || [channel]);
			}, 0);
			
		}
		ns.callbacks = ns.callbacks || [];
		return (ns.callbacks.push(callback) - 1);
	},

	unsubscribe: function (channel, subscription_index) {
		var ns = namespace(this._channels, channel);
		if ('callbacks' in ns) {
			ns.callbacks[subscription_index] = undefined;
			return true;
		}
		return false;
	},

	publish: function (channel) {
		var ns = namespace(this._channels, channel);

		if ('options' in ns && 'times' in ns.options && 'calls' in ns && ns.calls >= ns.options.times) {
			// event is no longer relevant, ignore
			return false;
		}

		// build callback arguments
		var args = slice.call(arguments, 1);
		args.push(channel); // channel name becomes the final argument
 		// save arguments
 		ns.args = args;

		var context = 'options' in ns ? ns.options.context : null;
		if ('callbacks' in ns) {
			each(ns.callbacks, function (callback) {
				setTimeout(function () {
					// callback is wrapped in a setTimeout so a thrown error won't break the queue
					callback && callback.apply(context, args);
				}, 0);
			});
		}
		ns.calls = ns.calls ? ns.calls + 1 : 1;
		return true;
	}
});

// promise resolution procedure
function _resolve (promise, x, fulfill, reject) {
	var then;
	var resolve_called, reject_called;

	if (promise === x) {
		reject(new TypeError('Cannot resolve a promise with itself'));
	}

	if (x instanceof Promise) {
		// x is a "native" promise
		x.then(fulfill, reject);
		return;
	}
	else if (x && x.then && typeof x.then == 'function') {
		// x is a promise-like object, reroute it through the resolver
		try {
			then = x.then;
		} catch (e) {
			reject(e);
			return;
		}

		try {
			then.call(x, function resolvePromise (y) {
				if (resolve_called || reject_called) {
					return;
				}
				resolve_called = true;
				_resolve(promise, y, fulfill, reject);
			}, function rejectPromise (r) {
				if (resolve_called || reject_called) {
					return;
				}
				reject_called = true;
				reject(r);
			});

		}
		catch (e) {
			if (!resolve_called && !reject_called) {
				reject(e);
			}
		}
		return;
	}
	else {
		// x is not promise-like, so fulfill the promise with x
		fulfill(x);
		return;
	}
}

var Promise = Prototype({
	constructor: function (resolver) {
		var done = false;
		var dispatch = new Mediator;
		var promise = this;

		dispatch.options('fulfill', { times: 1, expires: false });
		dispatch.options('reject', { times: 1, expires: false });

		function fulfiller (value) {
			if (!done) {
				done = true;
				dispatch.publish('fulfill', value);
			}
		}

		function rejecter (reason) {
			if (!done) {
				done = true;
				dispatch.publish('reject', reason);
			}
		}

		function then (onFulfilled, onRejected) {
			var deferred = new Deferred();

			dispatch.subscribe('fulfill', function (value) {
				try {
					if (isFunction(onFulfilled)) {
						_resolve(deferred.promise, onFulfilled(value), deferred.fulfill, deferred.reject);
					} else {
						deferred.fulfill(value);
					}
				} catch (e) {
					deferred.reject(e);
				}
			});
			
			dispatch.subscribe('reject', function (reason) {
				try {
					if (isFunction(onRejected)) {
						_resolve(deferred.promise, onRejected(reason), deferred.fulfill, deferred.reject);
					} else {
						deferred.reject(reason);
					}
				} catch (e) {
					deferred.reject(e);
				}
			});

			return deferred.promise;
		}

		promise.then = then;

		try {
			resolver(fulfiller, rejecter);
		} catch (e) {
			rejecter(e);
		}
	},

	// like a catch clause
	or: function (handler) {
		return this.then(undefined, handler);
	},

	resolveWith: function (value) {
		return this.then(function () {
			return value;
		});
	},

	rejectWith: function (reason) {
		return this.then(function () {
			throw reason;
		});
	},

	// like a finally clause
	regardless: function (handler) {
		var self = this;

		return this.then(function (value) {
			return self.then(handler).resolveWith(value);
		}, function (reason) {
			return self.then(handler).rejectWith(reason);
		});
	},

	// will throw any unhandled rejection rather than passing it along
	done: function () {
		this.then(undefined, function (reason) {
			setTimeout(function () {
				throw reason;
			}, 10);
		});
	},

	/** methods for promises for collections **/
	each: function (iterator, context) {
		return this.then(function (collection) {
			return each(collection, iterator, context);
		});
	},

	map: function (iterator, context) {
		return this.then(function (collection) {
			return map(collection, iterator, context);
		});
	},

	reduce: function (iterator, memo, context) {
		return this.then(function (collection) {
			return reduce(collection, iterator, memo, context);
		});
	},

	filter: function (iterator, context) {
		return this.then(function (collection) {
			return filter(collection, iterator, context);
		});
	}


});

function Deferred () {
	var deferred = {};

	deferred.promise = new Promise(function (fulfill, reject) {
		deferred.fulfill = fulfill;
		deferred.reject = reject;
	});

	return deferred;
}

function when (valueOrPromise, onFulfilled, onRejected) {
	var deferred = Deferred();
	_resolve(deferred.promise, valueOrPromise, deferred.fulfill, deferred.reject);
	return deferred.promise.then(onFulfilled, onRejected);
}

Promise.collect = function (inputs, fulfillmentOperator, rejectionOperator, context) {
	var deferred = new Deferred();
	var results = [];
	var l = inputs.length;
	var i = -1;
	var fulfilledCount = 0;

	while (++i < l) {
		var promise = inputs[i];
		when(promise, function (value) {
			fulfillmentOperator.call(context || null, value, deferred, results, l, ++fulfilledCount);
		}, function (reason) {
			rejectionOperator.call(context || null, reason, deferred, results, l, fulfilledCount);
		});
	}

	return deferred.promise;
};

function _allFulfillmentOperator (value, deferred, results, totalCount, fulfilledCount) {
	results.push(value);
	if (fulfilledCount >= totalCount) {
		deferred.fulfill(results);
	}
}

function _anyFulfillmentOperator (value, deferred, results, totalCount, fulfilledCount) {
	deferred.fulfill(value);
}

function _anyRejectionOperator (reason, deferred) {
	deferred.reject(reason);
}

Promise.all = function (inputs, onAllFulfilled, onRejected, context) {
	return Promise.collect(inputs, _allFulfillmentOperator, _anyRejectionOperator, context).then(onAllFulfilled, onRejected);
};

Promise.any = function (inputs, onFulfilled, onRejected, context) {
	return Promise.collect(inputs, _anyFulfillmentOperator, _anyRejectionOperator, context).then(onFulfilled, onRejected);
};

// delays the execution of a function by the supplied nuber of milliseconds,
// then calls it with the remaining arguments. returns a promise that will be
// fulfilled with the return value of the function.
function delay (fn, delay) {
	var deferred = new Deferred();
	var args = slice.call(arguments, 2);

	deferred.promise.timer = setTimeout(function () {
		try {
			deferred.fulfill(fn.apply(undefined, args));
		} catch (e) {
			deferred.reject(e);
		}
	}, delay);

	return deferred.promise;
}

// delays executing a function until the current callstack is cleared,
// then calls it with the remaining arguments
// TODO: could use setImmediate if available
function defer (fn) {
	return delay.apply(undefined, [fn, 1].concat(slice.call(arguments, 1)));
}

var rdx = {

	isArray: isArray,
	isFunction: isFunction,

	// function utilities
	bind: bind,
	unbind: unbind,
	partial: partial,
	throttle: throttle,
	debounce: debounce,

	// collection utilities
	has: has,
	getOwn: getOwn,
	keys: keys,
	values: values,
	each: each,
	map: map,
	reduce: reduce,
	filter: filter,
	mixin: mixin,
	extend: extend,
	create: create,
	merge: merge,
	invoke: invoke,
	size: size,
	pluck: pluck,

	// array utils
	range: range,
	shuffle: shuffle,

	// string utilities
	escapeRegExp: escapeRegExp,
	escapeHtml: escapeHtml,
	compile: compile,
	template: template,
	trim: trim,

	// misc utilities
	namespace: namespace,
	Prototype: Prototype,
	Mediator: Mediator,

	// async utilties
	Promise: Promise,
	Deferred: Deferred,
	when: when,
	delay: delay,
	defer: defer

};

if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
	global.rdx = rdx;
	define('rdx', function () { return rdx; });
}
else if (typeof exports !== 'undefined') {
	if (typeof module !== 'undefined' && 'exports' in module) {
		exports = module.exports = rdx;
	}
	exports.rdx = rdx;
}
else {
	global.rdx = rdx;
}

}(this));
var define;
(function(global) {
	'use strict';

	var modules = {}; // defined modules
	
	var pending_modules = {}; // modules waiting on dependencies
	var pending_deps = {}; // map of names of dependencies being waited on
	var pending_anon_count = 0; // for generating numeric uids for pending anonymous modules
	var pending_anons = {}; // anonymous modules waiting on dependencies (with generated numeric UIDs)

	/**
	 * determines the yet undefined dependencies from a module's array of dependencies
	 * @private
	 * @param  {[Array]} deps a module's array of dependencies
	 * @return {[Array]} an array containing the names of yet undefined dependencies
	 */
	function get_undefined_deps (deps) {
		var result = [];
		var l = deps.length;
		
		while (l--) {
			if (!(deps[l] in modules)) {
				// module not yet loaded
				result.push(deps[l]);
				pending_deps[deps[l]] = 1; // flag as a pending module's dependency
			}
		}

		return result;
	}

	/**
	 * builds a module once all its dependencies are ready
	 * @private
	 */
	function build (factory, deps, name) {
		var module;
		var args = [];
		var l = deps.length;
		var i = -1;

		// build arguments
		while (++i < l) {
			args.push(modules[deps[i]]);
		}

		module = factory.apply(undefined, args);

		if (typeof name == 'string') {
			// save the module
			modules[name] = module;
			ready(name);
		}
	}

	// called when a module is ready to be used
	// looks for modules dependent on the newly ready module, removes them as
	// pending dependencies, and builds modules with no remaining dependencies
	function ready (module_name) {
		// check waiting modules for this module as a dependency
		var pending_name;
		var pending_definition;
		var waiting_on;
		var dependency_name;
		var all_deps_ready;

		if (module_name in pending_deps) {
			// something is waiting on this module
			for (pending_name in pending_modules) {

				pending_definition = pending_modules[pending_name];

				all_deps_ready = dependency_ready(module_name, pending_definition);

				if (all_deps_ready) {
					if (pending_definition.is_function) {
						build(pending_definition.factory, pending_definition.deps, pending_name);
					} else {
						modules[module_name] = pending_definition.factory;
					}
				}
			}

			for (pending_name in pending_anons) {
				pending_definition = pending_anons[pending_name];

				all_deps_ready = dependency_ready(module_name, pending_definition);

				if (all_deps_ready && pending_definition.is_function) {
					build(pending_definition.factory, pending_definition.deps);
				}
			}
			
			delete pending_deps[module_name];
		}
	}
	
	// called when a dependency for a pending module becomes ready
	function dependency_ready (dep_name, pending_definition) {
		var all_deps_ready = false;
		var waiting_for = pending_definition.waiting_for
		var waiting_length = waiting_for.length;
		var l;
		
		if (dep_name in pending_definition.waiting_for_map) {
			delete pending_definition.waiting_for_map[dep_name];
			l = waiting_length;
			while (l--) {
				if (waiting_for[l] == dep_name) {
					// remove the dep
					waiting_for.splice(l, 1);
				}
			}
		}
		
		return waiting_for.length == 0;
	}


	define = function define (name, deps, factory) {
		var is_anonymous = false;
		var args_length = arguments.length;

		// juggle arguments
		if (args_length === 1) {
			// no name or dependencies
			is_anonymous = true;
			deps = [];
			factory = name;
		} 
		else if (args_length == 2) {
			// factory plus either name or deps
			factory = arguments[1];
			// figure out the other argument
			if (typeof name !== 'string') {
				// deps, but no name
				is_anonymous = true;
				deps = name;
			} else {
				// name, but no deps
				// name = name;
				factory = arguments[1];
				deps = [];
			}
		}

		// watch out for naming collisions
		if (!is_anonymous && modules.hasOwnProperty(name) || pending_modules.hasOwnProperty(name)) {
			throw('define: naming collision on `' + name + '`');
		}

		var dlen = deps.length;
		var is_function = typeof factory == 'function';
		var waiting_for = get_undefined_deps(deps);

		var waiting_for_map;
		var waiting_for_length = waiting_for.length;
		var waiting_for_map_i = -1;

		if (waiting_for_length) {
			// dependencies aren't all available
			waiting_for_map = {};

			while (++waiting_for_map_i < waiting_for_length) {
				waiting_for_map[waiting_for[waiting_for_map_i]] = 1;
			}


			var pending_definition = {
				factory: factory,
				deps: deps,
				waiting_for: waiting_for,
				waiting_for_map: waiting_for_map,
				is_function: is_function
			};

			if (is_anonymous) {
				pending_anons[pending_anon_count++] = pending_definition;
			}
			else {
				pending_modules[name] = pending_definition;
			}
		}
		else {
			if (is_function) {
				build(factory, deps, name);
			}
			else if (!is_anonymous) {
				// factory is just an object
				modules[name] = factory;
				ready(name);
			}
		}


	};

	define.amd = {};

	define.list = function list () {
		return Object.keys(modules);
	};

}(this));
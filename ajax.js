(function (global, redux) {
	'use strict';

	// TODO: add timeout option?

	var empty_function = function () {};
	var global_dispatch = new redux.Mediator; // for global ajax callbacks
	var open_count = 0; // the number of currently open requests

	function getOpenCount () { return open_count; }
	function inProgress () { return open_count > 0; }

	// track open requests
	global_dispatch.subscribe('start', function () { open_count++ });
	global_dispatch.subscribe('complete', function () { open_count-- });


	// setup default request configuration and dispatch options
	var default_config = redux.Base.extend({
		method: 'GET',
		cache: false,
		headers: {}
	});

	// special response type handlers
	var response_handlers = {

		'json' : function (xhr) {
			// automatically convert json response to an object
			return /^\s*$/.test(xhr.responseText) ? null : JSON.parse(xhr.responseText);
		},

		'js' : function (xhr) {
			// automatically evaluate javascript response in global scope
			(1,eval)(xhr.responseText);
			return xhr.responseText;
		}

	};

	var Request = redux.Constructor(function (opts) {
		var request = this;
		var xhr = new window.XMLHttpRequest();
		var deferred = new redux.Deferred();
		var promise = deferred.promise;

		// adapt promise interface
		request.then = promise.then;
		for (var method_name in redux.Promise.prototype) {
			request[method_name] = promise[method_name];
		}

		// build configuration from default
		var config = default_config.extend(opts);

		// protocol handling humbly borrowed from https://github.com/madrobby/zepto/blob/master/src/ajax.js
		var protocol = /^([\w-]+:)\/\//.test(config.url) ? RegExp.$1 : window.location.protocol;

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {

				// request completed
				xhr.onreadystatechange = empty_function;

				// trigger global ajax event
				global_dispatch.publish('complete');

				if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304 || (xhr.status === 0 && protocol === 'file:')) {
					// most likely successful
					
					try {
						var response = config.type && response_handlers[config.type] 
							? response_handlers[config.type](xhr)
							: xhr.responseText
						;

						deferred.fulfill(response);
						request.status = 'success';
						global_dispatch.publish('success', response, xhr, config);
					} catch (e) {
						request.status = 'error';
						deferred.reject(e);
						global_dispatch.publish('error', request.status, xhr, config);
					}

				} else {
					// error

					// set the status and trigger events
					request.status = xhr.status ? 'error' : 'abort';

					// dispatch.publish('error', request.status, xhr, config);
					deferred.reject(new Error(xhr.statusText));

					global_dispatch.publish('error', request.status, xhr, config);

				}
 
				global_dispatch.publish('complete', request.status, xhr, config);
			}
		};

		this.config = config;
		this.xhr = xhr;
	}, {
		
		send: function (params) {
			if (!params) params = this.config.params;
			global_dispatch.publish('start', this, params);
			var url = this.config.url;

			if (!this.config.cache) {
				var cache_buster = '__=' + (new Date).getTime();
				url += (/\?/.test(url) ? '&' : '?') + cache_buster;
			}

			this.xhr.open(this.config.method || 'GET', url, true);

			this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
			// tip: set config.contentType to false to skip setting this header
			if (this.config.method.toUpperCase() !== 'GET' && this.config.contentType !== false) {
				this.xhr.setRequestHeader('Content-type', (this.config.contentType || 'application/x-www-form-urlencoded'));
			}

			if (redux.isObject(this.config.headers)) {
				for (var key in this.config.headers) {
					this.xhr.setRequestHeader(key, this.config.headers[key]);
				}
			}

			// sends the request
			this.xhr.send(params);
			return this;
		}

	});

	/**
	 * create and immediately send an ajax request
	 * @param  {string} url optional
	 * @param  {Object} opts optional
	 * @return {redux.Request} the request object created
	 */
	function ajax (url, opts) {
		if (typeof url === 'string') {
			if (typeof opts !== 'object') {
				var opts = {};
			}
			opts.url = url;
		}

		var req = new Request(opts);

		req.send(opts.params || null);
		return req;
	}

	redux.ajax = ajax;
	redux.ajax.Request = Request;
	redux.ajax.getOpenCount = getOpenCount;
	redux.ajax.inProgress = inProgress;
 
})(this, this.redux);
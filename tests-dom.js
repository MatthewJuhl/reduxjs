describe('redux.dom', function () {
	var div = document.createElement('div');

	describe('#isNode', function () {
		it('returns true if the object is an element or a document fragment', function () {
			expect(redux.dom.isNode(div)).toBe(true);
			expect(redux.dom.isNode(document.createDocumentFragment())).toBe(true);
			expect(redux.dom.isNode(document.createTextNode())).toBe(false);
			expect(redux.dom.isNode({})).toBe(false);
			expect(redux.dom.isNode(0)).toBe(false);
			expect(redux.dom.isNode(null)).toBe(false);
			expect(redux.dom.isNode('foo')).toBe(false);
		});
	});
	
	describe('#eachDescendent', function () {
		it('performs a function on each descendent of a node', function () {
			var html = '<h1>f<small>o</small></h1><h2>o</h2>';
			div.innerHTML = html;
			var result = '';
			redux.dom.eachDescendent(div, function (node) {
				result += node.tagName;
			});
			// this may need to be modified, not sure if order is always the same
			expect(result).toBe('H1SMALLH2');
		});
	});

	describe('#addClass', function () {
		it('adds a class name to an element', function () {
			div.className = 'foo';
			redux.dom.addClass(div, 'bar');
			expect(div.className).toBe('foo bar');
		});
	});

	describe('#appendHtml', function () {
		it('adds a string of HTML to the end of an element', function () {
			var html = '<span><a href="#">Hello</a> Testing</span>';
			div.innerHTML = html;
			var appended = 'The End';
			redux.dom.appendHtml(div, appended);
			expect(div.innerHTML.toLowerCase()).toBe((html + appended).toLowerCase());
		});
	});

	describe('#prependHtml', function () {
		it('adds a string of HTML to the beginning of an element', function () {
			var html = '<span><a href="#">Hello</a> Testing</span>';
			div.innerHTML = html;
			var prepended = 'The Beginning';
			redux.dom.prependHtml(div, prepended);
			expect(div.innerHTML.toLowerCase()).toBe((prepended + html).toLowerCase());
		});
	});


	describe('#bind', function () {
		it('should attach an event listener to a node', function () {
			var flag;
			function f () { flag = true; }
			redux.dom.bind(div, 'click', f);
			div.click();
			expect(flag).toBe(true);
		});

		it('should pass a proper event object to the handler, and be called in the scope of the bound element', function () {


			var div2 = document.createElement('DIV');
			document.body.appendChild(div2);
			var thisDiv2;
			var target, currentTarget;
			var theEventObject;



			redux.dom.bind(div2, 'click', function (eventObject) {

				theEventObject = eventObject;
				thisDiv2 = this;
				target = eventObject.target;
				currentTarget = eventObject.currentTarget;
			});

			div2.click();
			expect(theEventObject).not.toEqual(undefined);
			expect(thisDiv2).toBe(div2);
			expect(target).toBe(div2);
			expect(currentTarget).toBe(div2);
		});

		it('should provide proper interface for preventing thd default action', function () {
			var checkbox = document.createElement('INPUT');
			checkbox.type = 'checkbox';
			redux.dom.bind(checkbox, 'click', function (click) {
				click.preventDefault();
			});
			checkbox.click();
			expect(checkbox.checked).toBe(false);
		});


	});

	describe('#unbind', function () {
		it('should remove an event listener from an element', function () {
			var elem = document.createElement('div');
			var flag = false, flag2;

			function f () { flag = true }
			redux.dom.bind(elem, 'click', f);
			redux.dom.unbind(elem, 'click', f);
			elem.click();
			expect(flag).toBe(false);
		});
	});

	describe('#trigger', function () {
		it('dipatches a simulated event on an element', function () {
			var elem = document.createElement('input');
			var flag, target, thisElem;

			document.body.appendChild(elem);
			redux.dom.bind(elem, 'focus', function (focus) {
				flag = true;
				target = focus.target;
				thisElem = this;
			});

			redux.dom.trigger(elem, 'focus');
			document.body.removeChild(elem);
			expect(flag).toBe(true);
			expect(target).toBe(elem);
			expect(thisElem).toBe(elem);
		});

		it('should trigger delegated events', function () {

			var flag;
			var outer = document.createElement('div');
			redux.dom.delegate(outer, 'click', 'span', function () {
				flag = true;
			});
			var inner = document.createElement('span');
			outer.appendChild(inner);
			document.body.appendChild(outer); // must do this for event to bubble
			redux.dom.trigger(inner, 'click');
			document.body.removeChild(outer);

			expect(flag).toBe(true);

		});
	});

	describe('#unbindAll', function () {
		it('should remove all event listeners from an element', function () {
			var elem = document.createElement('input');
			document.body.appendChild(elem);
			var flag = false, flag2;
			
			function f () { flag = true }
			function g () { flag = true }
			function h () { flag = true }

			// first, let an event be triggered to ensure triggering works
			// otherwise this test is meaningless
			redux.dom.bind(elem, 'click', function () {
				flag2 = true;
			});
			elem.click();
			

			redux.dom.bind(elem, 'click', f);
			redux.dom.bind(elem, 'click', g);
			redux.dom.bind(elem, 'blur', h);
			redux.dom.bind(elem, 'focus', h);
			redux.dom.unbindAll(elem);
			elem.click();
			elem.blur();
			elem.focus();
			document.body.removeChild(elem);
			expect(flag).toBe(false);
			expect(flag2).toBe(true);
		});
	});

	describe('#delegate', function () {
		it('adds a delegated event listener to an element', function () {
			var flag;
			var outer = document.createElement('div');
			redux.dom.delegate(outer, 'click', 'span', function () {
				flag = true;
			});
			var inner = document.createElement('span');
			outer.appendChild(inner);
			document.body.appendChild(outer); // must do this for event to bubble
			inner.click();
			document.body.removeChild(outer);

			expect(flag).toBe(true);
		});

		it('calls the callback in the scope of the original target', function () {
			var thisElem;
			var outer = document.createElement('div');

			redux.dom.delegate(outer, 'click', 'span', function () {
				thisElem = this;
			});
			var inner = document.createElement('span');
			outer.appendChild(inner);
			document.body.appendChild(outer); // must do this for event to bubble
			inner.click();
			document.body.removeChild(outer);

			expect(thisElem).toBe(inner);
		});

		it('passes a proper event object to the callback', function () {
			var target;
			var outer = document.createElement('div');


			redux.dom.delegate(outer, 'click', 'span', function (e) {
				target = e.srcElement;
			});
			var inner = document.createElement('span');
			outer.appendChild(inner);
			document.body.appendChild(outer); // must do this for event to bubble
			inner.click();
			document.body.removeChild(outer);

			expect(target).toBe(inner);
		});
	});

	describe('#undelegate', function () {
		it('removes a delegated event listener to an element', function () {
			var flag = false;
			var outer = document.createElement('div');
			function f () { flag = true }
			redux.dom.delegate(outer, 'click', 'span', f);
			var inner = document.createElement('span');
			outer.appendChild(inner);
			document.body.appendChild(outer); // must do this for event to bubble
			redux.dom.undelegate(outer, 'click', 'span', f);
			inner.click();
			document.body.removeChild(outer);

			expect(flag).toBe(false);
		});
	});



});
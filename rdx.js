(function (global, undefined) {
'use strict';

var keys = Object.keys;
var slice = [].slice;

function each (collection, iterator, context) {
	if (collection == null) return collection;

	var length = collection.length;
	var index = -1;
	var key;
	var collection_keys;

	if (length === +length) {
		// treat the collection like an array
		while (++index < length) {
			// call the iterator, and break if it returns false
			if (iterator.call(context, collection[index], index, collection) === false) {
				break;
			}
		}
	} else {
		// treat the collection like an object
		collection_keys = keys(collection);
		length = collection_keys.length;

		while (++index < length) {
			key = collection_keys[index];
			// call the iterator, and break if it returns false
			if (iterator.call(context, collection[key], key, collection) === false) {
				break;
			}
		}
	}
}

function map (collection, iterator, context) {
	var results = [];
	var length = collection.length;
	var index = -1;

	if (length === +length) {
		while (++index < length) {
			results.push(iterator.call(context, collection[index], index, collection))
		}
	} else {
		each(collection, function (prop, key, collection) {
			results.push(iterator.call(context, prop, key, collection));
		}, context);
	}
	return results;
}

function reduce (collection, iterator, memo, context) {
	var init = arguments.length > 2;

	each(collection, function (val, key, list) {
		if (!init) {
			memo = val;
			init = true;
		} else {
			memo = iterator.call(context, memo, val, key, collection);
		}
	});

	return memo;
}

function filter (collection, iterator, context) {
	var length = collection.length;
	var index = -1;
	var result, collection_keys, key, val;

	if (length === +length) {
		result = [];
		while (++index < length) {
			if (iterator.call(context, collection[index], index, collection)) {
				result.push(collection[index]);
			}
		}
	} else {
		result = {};
		collection_keys = keys(collection);
		length = collection_keys.length;
		while (++index < length) {
			key = collection_keys[index];
			val = collection[key];
			if (iterator.call(context, val, key, collection)) {
				result[key] = val;
			}
		}
	}
	return result;
}

function extend (object, source) {
	var own_keys = keys(source);
	var l = own_keys.length;
	var i = -1;
	var key;

	while (++i < l) {
		key = own_keys[i];
		object[key] = source[key];
	}

	// each(source, function (prop, key, source) {
	// 	object[key] = prop;
	// });
	return object;
}

function size (collection) {
	if (collection.length === +collection.length) {
		return collection.length;
	}
	return keys(collection).length;
}

// gets the values of a collection as an array
// (bonus: will return a shallow clone of an array)
function values (collection) {
	var length = size(collection);
	var own_keys = keys(collection);
	var index = -1;
	var result = [];

	while (++index < length) {
		result.push(collection[own_keys[index]]);
	}
	return result;
}

// Fisher–Yates Shuffle
// this is cool: http://bost.ocks.org/mike/shuffle/
function shuffle (collection) {
	// copy the array (or the object's values)
	var result = values(collection);
	var length = result.length;
	var temp, rand;

	while (length) {
		// get a random int between 0 and the number of remaining unshuffled values
		rand = Math.floor(Math.random() * length--);
		
		// swap the last unshuffled value with a randomly selected unshuffled value
		temp = result[length];
		result[length] = result[rand];
		result[rand] = temp;
	}

	return result;
}

function invoke (collection, method) {
	var args = [].slice.call(arguments, 2);
	var fun = typeof method == 'function';

	return map(collection, function (val) {
		return (fun? method : val[method]).apply(val, args);
	});
}

function pluck (collection, property) {
	return map(collection, function (val) {
		return val[property];
	});
}

function namespace (context, path, props) {

	if (context == null) return;

	var levels = path.split('.');
	var l = levels.length;
	var i = -1;
	var namespace = context;

	while (++i < l) {
		if (namespace[levels[i]] == null) {
			namespace[levels[i]] = {};
		}

		namespace = namespace[levels[i]];
	}

	extend(namespace, props);
	return namespace;
}

// a convenience method for binding a prototype and constructor together.
// if the prototype doesn't contain a constructor function, an empty one
// will be used.
function proto (prototype) {
	var constructor = prototype.constructor || new Function;
	constructor.prototype = prototype;
	return constructor;
}

var Mediator = proto({

	constructor: function () {
		this._channels = {};
	},

	// set or augment a channels options
	// available options:
	// 	(obj) 'context' the context in which callbacks should be executed
	// 	(int) 'times' the number of times an event is allowed to occur
	// 	(bool) 'expires' if true, callbacks added after an event has occurred 
	// 	                 the allowed number of times will be ignored, otherwise
	// 	                 they will be immediately executed with the same 
	// 	                 arguments as the final occurance
	// 	
	options: function (channel, options) {
		namespace(this._channels, channel + '.options', options);
	},

	subscribe: function (channel, callback) {
		var ns = namespace(this._channels, channel);
		var context = ns.options ? ns.options.context : null;
		if ('options' in ns && 'times' in ns.options && 'calls' in ns && ns.calls >= ns.options.times) {
			// event is no longer relevant, call immediately
			setTimeout(function () {
				callback.apply(context, ns.args || [channel]);
			}, 0);
			
		}
		ns.callbacks = ns.callbacks || [];
		return (ns.callbacks.push(callback) - 1);
	},

	unsubscribe: function (channel, subscription_index) {
		var ns = namespace(this._channels, channel);
		if ('callbacks' in ns) {
			ns.callbacks[subscription_index] = undefined;
			return true;
		}
		return false;
	},

	publish: function (channel) {
		var ns = namespace(this._channels, channel);

		if ('options' in ns && 'times' in ns.options && 'calls' in ns && ns.calls >= ns.options.times) {
			// event is no longer relevant, ignore
			return false;
		}

		// build callback arguments
		var args = slice.call(arguments, 1);
		args.push(channel); // channel name becomes the final argument
 		// save arguments
 		ns.args = args;

		var context = 'options' in ns ? ns.options.context : null;
		if ('callbacks' in ns) {
			each(ns.callbacks, function (callback) {
				setTimeout(function () {
					// callback is wrapped in a setTimeout so a thrown error won't break the queue
					callback && callback.apply(context, args);
				}, 0);
			});
		}
		ns.calls = ns.calls ? ns.calls + 1 : 1;
		return true;
	}
});

// promise resolution procedure
function _resolve (promise, x, fulfill, reject) {
	var then;
	var resolve_called, reject_called;

	if (promise === x) {
		reject(new TypeError('Cannot resolve a promise with itself'));
	}

	if (x instanceof Promise) {
		// x is a "native" promise
		x.then(fulfill, reject);
		return;
	} else if (x && x.then && typeof x.then == 'function') {
		// x is a promise-like object, reroute it through the resolver
		try {
			then = x.then;
		} catch (e) {
			reject(e);
			return;
		}

		try {
			then.call(x, function resolvePromise (y) {
				if (resolve_called || reject_called) {
					return;
				}
				resolve_called = true;
				_resolve(promise, y, fulfill, reject);
			}, function rejectPromise (r) {
				if (resolve_called || reject_called) {
					return;
				}
				reject_called = true;
				reject(r);
			});

		} catch (e) {
			if (!resolve_called && !reject_called) {
				reject(e);
			}
		}
		return;
	} else {
		// x is not promise-like, so fulfill the promise with x
		fulfill(x);
		return;
	}
}

var Promise = proto({
	constructor: function (resolver) {
		var done = false;
		var dispatch = new Mediator;
		var promise = this;

		dispatch.options('fulfill', { times: 1, expires: false });
		dispatch.options('reject', { times: 1, expires: false });

		function fulfiller (value) {
			if (!done) {
				done = true;
				dispatch.publish('fulfill', value);
			}
		}

		function rejecter (reason) {
			if (!done) {
				done = true;
				dispatch.publish('reject', reason);
			}
		}

		function then (onFulfilled, onRejected) {
			var deferred = new Deferred();

			dispatch.subscribe('fulfill', function (value) {
				try {
					if (typeof onFulfilled == 'function')) {
						_resolve(deferred.promise, onFulfilled(value), deferred.fulfill, deferred.reject);
					} else {
						deferred.fulfill(value);
					}
				} catch (e) {
					deferred.reject(e);
				}
			});
			
			dispatch.subscribe('reject', function (reason) {
				try {
					if (typeof onRejected == 'function') {
						_resolve(deferred.promise, onRejected(reason), deferred.fulfill, deferred.reject);
					} else {
						deferred.reject(reason);
					}
				} catch (e) {
					deferred.reject(e);
				}
			});

			return deferred.promise;
		}

		promise.then = then;

		try {
			resolver(fulfiller, rejecter);
		} catch (e) {
			rejecter(e);
		}
	},

	// like a catch clause
	or: function (handler) {
		return this.then(undefined, handler);
	},

	resolveWith: function (value) {
		return this.then(function () {
			return value;
		});
	},

	rejectWith: function (reason) {
		return this.then(function () {
			throw reason;
		});
	},

	// like a finally clause
	regardless: function (handler) {
		var self = this;

		return this.then(function (value) {
			return self.then(handler).resolveWith(value);
		}, function (reason) {
			return self.then(handler).rejectWith(reason);
		});
	},

	// will throw any unhandled rejection rather than passing it along
	done: function () {
		this.then(undefined, function (reason) {
			setTimeout(function () {
				throw reason;
			}, 10);
		});
	},

	/** methods for promises that resolve with collections **/
	each: function (iterator, context) {
		return this.then(function (collection) {
			return each(collection, iterator, context);
		});
	},

	map: function (iterator, context) {
		return this.then(function (collection) {
			return map(collection, iterator, context);
		});
	},

	reduce: function (iterator, memo, context) {
		return this.then(function (collection) {
			return reduce(collection, iterator, memo, context);
		});
	},

	filter: function (iterator, context) {
		return this.then(function (collection) {
			return filter(collection, iterator, context);
		});
	}
});

function Deferred () {
	var deferred = {};

	deferred.promise = new Promise(function (fulfill, reject) {
		deferred.fulfill = fulfill;
		deferred.reject = reject;
	});

	return deferred;
}

// when is a syntactic shortcut to _resolve
function when (valueOrPromise, onFulfilled, onRejected) {
	var deferred = Deferred();
	_resolve(deferred.promise, valueOrPromise, deferred.fulfill, deferred.reject);
	return deferred.promise.then(onFulfilled, onRejected);
}

Promise.collect = function (inputs, fulfillmentOperator, rejectionOperator, context) {
	var deferred = new Deferred();
	var results = [];
	var l = inputs.length;
	var i = -1;
	var fulfilledCount = 0;

	while (++i < l) {
		var promise = inputs[i];
		when(promise, function (value) {
			fulfillmentOperator.call(context || null, value, deferred, results, l, ++fulfilledCount);
		}, function (reason) {
			rejectionOperator.call(context || null, reason, deferred, results, l, fulfilledCount);
		});
	}

	return deferred.promise;
};

function _allFulfillmentOperator (value, deferred, results, totalCount, fulfilledCount) {
	results.push(value);
	if (fulfilledCount >= totalCount) {
		deferred.fulfill(results);
	}
}

function _anyFulfillmentOperator (value, deferred, results, totalCount, fulfilledCount) {
	deferred.fulfill(value);
}

function _anyRejectionOperator (reason, deferred) {
	deferred.reject(reason);
}

Promise.all = function (inputs, onAllFulfilled, onRejected, context) {
	return Promise.collect(inputs, _allFulfillmentOperator, _anyRejectionOperator, context).then(onAllFulfilled, onRejected);
};

Promise.any = function (inputs, onFulfilled, onRejected, context) {
	return Promise.collect(inputs, _anyFulfillmentOperator, _anyRejectionOperator, context).then(onFulfilled, onRejected);
};

// delays the execution of a function by the supplied nuber of milliseconds,
// then calls it with the remaining arguments. returns a promise that will be
// fulfilled with the return value of the function.
function delay (fn, delay) {
	var deferred = new Deferred();
	var args = slice.call(arguments, 2);

	deferred.promise.timer = setTimeout(function () {
		try {
			deferred.fulfill(fn.apply(undefined, args));
		} catch (e) {
			deferred.reject(e);
		}
	}, delay);

	return deferred.promise;
}

// delays executing a function until the current callstack is cleared,
// then calls it with the remaining arguments
function defer (fn) {
	return delay.apply(undefined, [fn, 1].concat(slice.call(arguments, 1)));
}

// returns a function that can only be called once during a specified interval
function throttle (fn, interval) {
	var last; // the last time the function was executed
	return function () {
		var context = this, args = arguments;
		var now = new Date;
		// execute if not previously executed or if enough time has elapsed
		if (!last || now - last >= interval) {
			last = now;
			return fn.apply(context, args);
		}
	};
}

// returns a function that will only execute after it has stopped being
// called for a specified amount of time
function debounce (fn, delay) {
	var scheduled;
	return function () {
		var context = this, args = arguments;
		clearTimeout(scheduled); // cancel previously scheduled execution
		scheduled = setTimeout(function () {
			fn.apply(context, args);
		}, delay);
	};
}

var rdx = {
	extend: extend,

	each: each,
	map: map,
	reduce: reduce,
	filter: filter,
	size: size,
	values: values,
	shuffle: shuffle,
	invoke: invoke,
	pluck: pluck,

	namespace: namespace,
	proto: proto,
	Mediator: Mediator,

	Promise: Promise,
	Deferred: Deferred,
	when: when,
	delay: delay,
	defer: defer,

	throttle: throttle,
	debounce: debounce
};

if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
	define('rdx', function () { return rdx; });
}
else if (typeof exports !== 'undefined') {
	if (typeof module !== 'undefined' && 'exports' in module) {
		exports = module.exports = rdx;
	}
	exports.rdx = rdx;
}
else {
	global.rdx = rdx;
}
}(this));
(function (global, factory) {
	'use strict';
 
	if (typeof exports !== 'undefined') {
		// Node/CommonJS
		var redux = factory();
		redux.extend(exports, redux);
	} else if (typeof define === 'function' && define.amd) {
		// AMD/RequireJS
		define('redux', [], factory);
	} else {
		// Browser global
		global.redux = factory();
	}
 
})(this, function () {
	'use strict';
 	
 	// Support: IE8
	// patch Function.prototype.bind
	if (!Function.prototype.bind) {
		Function.prototype.bind = function (oThis) {
			if (typeof this !== "function") {
				// closest thing possible to the ECMAScript 5 internal IsCallable function
				throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
			}
 
			var aArgs = Array.prototype.slice.call(arguments, 1);
			var fToBind = this;
			var fNOP = function () {};
			var fBound = function () {
				return fToBind.apply(this instanceof fNOP && oThis
					                    ? this
					                    : oThis,
					                  aArgs.concat(Array.prototype.slice.call(arguments)));
			};
 
			fNOP.prototype = this.prototype;
			fBound.prototype = new fNOP();
 
			return fBound;
		};
	}

	// Support: IE8
	// patch String.prototype.trim
	if(!String.prototype.trim) {
		String.prototype.trim = function () {
			return this.replace(/^\s+|\s+$/g,'');
		};
	}
 	
 	var native_create = Object.create;
	var native_keys = Object.keys;
 	var object_proto = Object.prototype;
 	var supports_proto = object_proto.__proto__ === null;
	var to_string = object_proto.toString;
	var array = [];
	var slice = array.slice;
	var native_each = array.each;
	var native_map = array.map;
	var native_reduce = array.reduce;
	var native_filter = array.filter;
	var empty_function = function () {};
 
	// some convenience wrappers
	var bind = function (func, context) {
		return Function.prototype.bind.call(func, context);
	};
 
	var has_own = function (obj, key) {
		return object_proto.hasOwnProperty.call(obj, key);
	};

	// borrowed humbly from underscore.js
	var isFunction = typeof (/./) !== 'function' 
		? function (thing) {
			return typeof thing == 'function';
		}

		: function (thing) {
			return to_string.call(thing) == '[object Function]';
		}
	;

	function isArray (thing) {
		return to_string.call(thing) == '[object Array]';
	}

	function isObject (thing) {
		return thing === Object(thing);
	}
 
	// patch Object.create(null) functionality
	var create_null;
	if (supports_proto) {
		// use __proto__ if possible
		create_null = function create_null () {
			return {
				'__proto__': null
			};
		}
	} else {
		// can't use __proto__, probably meaning older IE
		create_null = (function () {
			// if available, use document to create an iframe
			var d = document;
			if (!d) {
				// nothing to do
				return empty_function;
			}
			var root = d.body || d.documentElement;
			var iframe = d.createElement('iframe');
			iframe.style.display = 'none';
			root.appendChild(iframe);
			iframe.src = 'javascript:';
			// steal the iframe's Object.prototype
			var proxy_object_proto = iframe.contentWindow.Object.prototype;
			root.removeChild(iframe);
			iframe = null;
 
			var keys = [
				'constructor',
				'hasOwnProperty',
				'isPrototypeOf',
				'propertyIsEnumerable',
				'toLocaleString',
				'toString',
				'valueOf'
			];
 
			// nullify the prototype
			each(keys, function (key) {
				delete proxy_object_proto[key];
			});
 
			// delete proxy_object_proto.constructor;
			// delete proxy_object_proto.hasOwnProperty;
			// delete proxy_object_proto.isPrototypeOf;
			// delete proxy_object_proto.propertyIsEnumerable;
			// delete proxy_object_proto.toLocaleString;
			// delete proxy_object_proto.toString;
			// delete proxy_object_proto.valueOf;
			proxy_object_proto.__proto__ = undefined;
 
			function N () {}
			N.prototype = proxy_object_proto;
 
			return function () {
				return new N();
			};
		}());
	}
	
	/**
	 * add all properties of source to destination
	 * @param  {Object} destination
	 * @param  {Object} source
	 * @return {void}
	 */
	function extend (destination, source) {
		for (var key in source) {
			if (has_own(source, key)) {
				destination[key] = source[key];
			}
		}
	}

	/**
	 * clone an object
	 * @param  {Object}	obj the object to clone
	 * @return {Object}	the cloned object
	 */
	// TODO: consider removing this
	function clone (obj) {
		// use native JSON to deep clone objects
		// IE8 has native JSON, so no need to patch
		// see http://jsperf.com/cloning-an-object/2
		return JSON.parse(JSON.stringify( obj ));
	}
 	
	/**
	 * creates a new object using an existing object as a prototype
	 * @param  {Object} prototype
	 * @param {Object or Function} props an object containing properties or a
	 *                             function that returns an object containing
	 *                             properties to add to the new object
	 * @return {Object}
	 */
	var empty_constructor = function () {};

	function create (proto, props) {
		var obj;

		if (isFunction (props)) {
			// props is a factory function
			// inspired by:
			// http://tagneto.blogspot.com/2010/04/javascript-object-inheritance-with.html
			props = props(function parent (method_name) {
				// the factory method will be passed a function for calling methods
				// from the prototype object, essentially as a shortcut for
				// proto[method].call(this, args) for overridden methods
				// used as parent('some_method', arg1, arg2, argN);
				var args = slice.call(arguments, 1);
				proto[method_name].apply(obj, slice.call(arguments, 1));
			});
		}
		
		if (native_create) {
			// default to native Object.create
			obj = native_create(proto);
		} else if (proto === null) {
			// create an object with a null prototype
			obj = create_null();
		} else {
			empty_constructor.prototype = proto;
			obj = new empty_constructor();
			empty_constructor.prototype = null;
		}
 
		// add properties
		extend(obj, props);
		return obj;
	}
 
	var Base = create({}, {
		extend: function (props) {
			return create(this, props);
			
			// var obj = create(this);
			// extend(obj, props);
			// return obj;
		}
	});
 
 	
	function partial (f) {
		if (arguments.length === 1) {
			return f;
		}
 
		var args = slice.call(arguments, 1);
		return function () {
			return f.apply(this, args.concat(slice.call(arguments)));
		}
	}

	// delays the execution of a function by the supplied nuber of milliseconds,
	// then calls it with the remaining arguments
	function delay (fn, delay) {
		var deferred = new Deferred();
		var args = slice.call(arguments, 2);

		deferred.promise.timer = setTimeout(function () {
			try {
				deferred.fulfill(fn.apply(null, args));
			} catch (e) {
				deferred.reject(e);
			}
		}, delay);

		return deferred.promise;
	}

	// delays executing a function until the current callstack is cleared,
	// then calls it with the remaining arguments
	function defer (fn) {
		return delay.apply(null, [fn, 10].concat(slice.call(arguments, 1)));
	}

	function wrap (f, wrapper) {
		return function () {
			wrapper.apply(this, [f].concat(slice.call(arguments)));
		};
	}

	// returns a function that can only be called once during a specified interval
	function throttle (fn, interval) {
		var last; // the last time the function was executed
		return function () {
			var context = this, args = arguments;
			var now = new Date;
			// execute if not previously executed or if enough time has elapsed
			if (!last || now - last >= interval) {
				last = now;
				return fn.apply(context, args);
			}
		};
	}

	// returns a function that will only execute after it has stopped being
	// called for a specified amount of time
	function debounce (fn, delay) {
		var scheduled;
		return function () {
			var context = this, args = arguments;
			clearTimeout(scheduled); // cancel previously scheduled execution
			scheduled = setTimeout(function () {
				fn.apply(context, args);
			}, delay);
		};
	}
	
 	/**
 	 * builds a constructor function using the supplied function and prototype
 	 * @param {Function} constructor the function to use as the constructor
 	 * @param {Object}   proto       the object to use as the prototype
 	 */
	function Constructor (constructor, proto) {
		extend(constructor.prototype, proto);
		return constructor;
	}
 
	function each (collection, iterator, context, break_on_false) {
		var length = collection.length;
		var i = -1;
		var value;
 	
 		// if a length prop exists, we can treat the collection as an array
		if (length === +length) {
			// use native forEach if available, unless we want to break on false
			// TODO: remove use of native forEach for possible performance gain
			if (native_each && !break_on_false) {
				native_each.call(collection, iterator, context);
			} else {
				while (++i < length) {
					value = iterator.call(context, collection[i], i, collection);
					if (break_on_false && value === false) {
						break;
					}
				}
			}
		} else {
			// not array like
			for (var key in collection) {
				if (has_own(collection, key)) {
					value = iterator.call(context, collection[key], key, collection);
				}
				if (break_on_false && value === false) {
					break;
				}
			}
		}
	}
 
	function map (collection, iterator, context) {
		var map = [];
		
		if (collection == null) {
			return map;
		}

		if (native_map && native_map === collection.map) {
			return collection.map(iterator, context);
		}
 
		each(collection, function (val, key, collection) {
			map.push(iterator.call(context, val, key, collection));
		}, context);

		return map;
	}
 
	function reduce (collection, iterator, memo, context) {

		var init = arguments.length > 2;
		
		if (native_reduce && collection.reduce === native_reduce) {
			// native reduce doesn't take a context argument,
			// so if one is passed here, bind the iterator to it
			if (context) iterator = bind(iterator, context);

			// only pass an initial value to the native reduce function if it was
			// specified here
			return init ? 
				collection.reduce(iterator, memo) : collection.reduce(iterator);
		}
			
		if (collection == null) {
			collection = [];
		}

		each(collection, function (val, key, list) {
			if (!init) {
				memo = val;
				init = true;
			} else {
				memo = iterator.call(context, memo, val, key, collection);
			}
		});

		if (!init) {
			throw new TypeError('Cannot reduce an empty array with no initial value');
		}

		return memo;		
	}
 
	/* * */
 
	function filter (collection, iterator, context) {
		var results = [];
		if (collection == null) return results;

		if (native_filter && collection.filter === native_filter) {
			return collection.filter(iterator, context);
		}
 
		each(collection, function(val, key, collection) {
			if (iterator.call(context, val, key, collection)) results.push(val);
		});
		return results;
	}
	 
	function invoke (collection, method) {
		var args = slice.call(arguments, 2);
		var fun = isFunction(method);
 
		return map(collection, function (val) {
			return (fun? method : val[method]).apply(val, args);
		});
	}
	 
	var keys = native_keys || function (collection) {
		if (collection !== Object(collection)) {
			throw new TypeError('keys cannot be called on a non-object.');
		}
		var keys = [];
		for (var key in collection) {
			if (has_own(collection, key)) {
				keys.push(key);
			}
		}
		return keys;
	};
	 
	function size (collection) {
		if (collection.length === +collection.length) {
			return collection.length;
		}
		return keys(collection).length;
	}
 
	function pluck (collection, property) {
		return map(collection, function (val) {
			return val[property];
		});
	}
 
 	/* * */
	var Hash = Constructor(function (props) {
		this.data = create(null, props);
	}, {
		
		set: function (key, value) {
			this.data[key] = value;
		},
 
		get: function (key) {
			return this.data[key];
		},
 
		each : function (iterator, context, break_on_false) {
			if (!context) {
				context = this;
			}
			return each.call(this, this.data, function (v, k, h) {
				// wrapping the iterator here to pass the hash as the third arg instead of hash.data
				iterator.call(this, v, k, this); 
			}, context, break_on_false);
		}
 
	});
 
	/**
	 * recursively creates a namespace within context
	 * if the ns already exists, it will be extended with the content
	 * example:
 
	redux.Namespace(window, 'SomeVendor.SomeModule', {
		someMethod: function () {}
	});
 
	**/
 
	function Namespace (context, path, content) {

		if (!context) return;

		var levels = path.split('.');
		var l = levels.length;
		var i;
		var namespace = context;
 
		for (i = 0; i < l; i++) {
			if (!namespace[levels[i]]) {
				namespace[levels[i]] = {}
			}
			
			namespace = namespace[levels[i]];
		}
 
		extend(namespace, content);
		return namespace;
	}

	var Mediator = Constructor(function () {
		this._channels = {};
	}, {
		// set channel options
		options: function (channel, options) {
			Namespace(this._channels, channel + '.options', options);
		},

		subscribe: function (channel, callback) {
			var ns = Namespace(this._channels, channel);
			var context = ns.options ? ns.options.context : null;
			if (ns.options && ns.options['times'] && ns.calls && ns.calls >= ns.options.times) {
				// event is no longer relevant, call immediately
				setTimeout(function () {
					callback.apply(context, ns.args || [channel]);
				}, 0);
				
			}
			ns.callbacks = ns.callbacks || [];
			return (ns.callbacks.push(callback))-1;
		},
	 
		unsubscribe: function (channel, subscription_index) {
			var ns = Namespace(this._channels, channel);
			if (ns.callbacks) {
				ns.callbacks[subscription_index] = undefined;
				return true;
			}
			return false;
		},
	 
		publish: function (channel, data) {
			var ns = Namespace(this._channels, channel);

			if (ns.options && ns.options['times'] && ns.calls && ns.calls >= ns.options.times) {
				// event is no longer relevant, ignore
				return false;
			}

			// build callback arguments
			var args = [].slice.call(arguments, 1);
			args.push(channel); // channel name becomes the final argument
	 		// save arguments
	 		ns.args = args;

	 		var context = ns.options ? ns.options.context : null;
			
			if (ns.callbacks) {
				each(ns.callbacks, function (callback) {
					setTimeout(function () {
						// callback is wrapped in a setTimeout so an error won't break the queue
						if (typeof callback === 'function') {
							callback.apply(context, args);
						}
					}, 0);
				});
			}

			ns.calls = ns.calls ? ns.calls + 1 : 1;
			return true;
		}
	});


	/**
	 * This promise implementation is fully compliant with the Promises/A+ spec
	 * <http://promises-aplus.github.com/promises-spec>
	 * Inspired by both Q.js and when.js
	 * <https://github.com/kriskowal/q>
	 * <https://github.com/cujojs/when>
	 */
	
	function Promise (resolver) {
		if (!this instanceof Promise) { return new Promise(resolver); }
		var promise1 = this;
		var is_done;
		
		// setup event dispatch for promise
		var dispatch = new Mediator;
		dispatch.options('fulfill', { times: 1 });
		dispatch.options('reject', { times: 1 });

		// the fulfill function to be passed to the resolver
		function fulfillPromise1 (value) {
			if (is_done) return;
			is_done = 1;
			dispatch.publish('fulfill', value);
		}

		// the reject function to be passed to the resolver
		function rejectPromise1 (reason) {
			if (is_done) return;
			is_done = 1;
			dispatch.publish('reject', reason);
		}

		promise1.then = function then (onFulfilled, onRejected) {
			// the 'then' function must return a new promise
			var fulfillPromise2, rejectPromise2;
			var promise2 = new Promise(function (fulfill, reject) {
				fulfillPromise2 = fulfill;
				rejectPromise2 = reject;
			});

			// attach the onFulfilled handler
			dispatch.subscribe('fulfill', function (value) {
				try {
					if (typeof onFulfilled == 'function') {
						var returnValue = onFulfilled(value);
						resolution_procedure(promise2, returnValue, fulfillPromise2, rejectPromise2);
					} else {
						// onFulfilled is not a function, so fulfill promise2
						// with the same value
						fulfillPromise2(value);
					}
				} catch (e) {
					rejectPromise2(e);
				}
			});

			// attach the onRejected handler
			dispatch.subscribe('reject', function (reason) {
				// promise1 has been rejected
				// attempt to call the onRejected function
				try {
					if (typeof onRejected == 'function') {
						var returnValue = onRejected(reason);
						resolution_procedure(promise2, returnValue, fulfillPromise2, rejectPromise2);
					} else {
						// onRejected is not a function, so reject
						// promise2 with the same reason
						rejectPromise2(reason);
					}
				} catch (e) {
					// the onRejected handler threw an error
					// so reject promise2
					rejectPromise2(e);
				}
			});

			return promise2;
		};

		// pass the fulfill/reject handlers to the resolver argument
		try {
			resolver(fulfillPromise1, rejectPromise1);
		} catch (e) {
			rejectPromise1(e);
		}
	}

	function resolution_procedure (promise, x, fulfill, reject) {
		// "If promise and x refer to the same object, reject promise with a TypeError as the reason."
		if (promise === x) {
			reject(new TypeError('Cannot resolve a promise with itself'));
		}

		// "If x is a promise, adopt its state [4.4]"
		if (x instanceof Promise) {
			// here, x is a native promise
			// "If x is pending, promise must remain pending until x is fulfilled or rejected."
			// "If/when x is fulfilled, fulfill promise with the same value."
			// "If/when x is rejected, reject promise with the same reason."
			x.then(fulfill, reject);
			return;

		} else if (x && x.then && typeof x.then == 'function') {
			// x is promise-like, possibly a promise from another library
			// "Otherwise, if x is an object or function,"
			var then;
			try {
				// "Let `then` be x.then"
				then = x.then;
			} catch (e) {
				// "If retrieving the property x.then results in a thrown exception e, reject promise with e as the reason."
				reject(e);
				return;
			}

			// "If then is a function, call it with x as this, first argument resolvePromise, and second argument rejectPromise, where:"
			var resolvePromiseCalled, rejectPromiseCalled;

			try {
				then.call(x, function resolvePromise (y) {
					if (resolvePromiseCalled || rejectPromiseCalled) {
						// "If both resolvePromise and rejectPromise are called, or multiple calls to the same argument are made, the first call takes precedence, and any further calls are ignored."
						return;
					}
					resolvePromiseCalled = 1;
					// "If/when resolvePromise is called with a value y, run [[Resolve]](promise, y)."
					resolution_procedure(promise, y, fulfill, reject);

				}, function rejectPromise (r) {
					if (resolvePromiseCalled || rejectPromiseCalled) {
						// "If both resolvePromise and rejectPromise are called, or multiple calls to the same argument are made, the first call takes precedence, and any further calls are ignored."
						return;
					}
					rejectPromiseCalled = 1;
					// "If/when rejectPromise is called with a reason r, reject promise with r."
					reject(r);
				});

			} catch (e) {
				// "If calling then throws an exception e,"
				// "If resolvePromise or rejectPromise have been called, ignore it."
				if (!resolvePromiseCalled && !rejectPromiseCalled) {
					// "Otherwise, reject promise with e as the reason."
					reject(e);
				}
			}
			return;
		} else {
			// "If x is not an object or function, fulfill promise with x."
			fulfill(x);
			return;
		}
	}

	// accepts a promise or a value, returns a promise to be fulfilled 
	// "immediately" if it's a value, or if it's a promise, when that promise
	// is resolved
	// ...err...
	// (returns a resolved promise with valueOrPromise if it's a value, or the
	//  resolved value of valueOrPromise if it's a promise)
	Promise.resolve = function (valueOrPromise) {

		var d = new Deferred();
		// if it's a value, we want to wait until after the current call stack
		// but due to the internal implementation using Mediator, this happens anyway
		// setTimeout(function () {
			resolution_procedure(d.promise, valueOrPromise, d.fulfill, d.reject);
		// }, 10);
		return d.promise;

		// return new Promise(function (resolve) {
		// 	resolve(valueOrPromise);
		// });

		// not sure wtf this was about
		// var deferred = new Deferred();
		// resolution_procedure(deferred.promise, valueOrPromise, deferred.fulfill, deferred.reject);
		// return deferred.promise;
	};

	// returns an already rejected promise with the given reason
	Promise.reject = function (reason) {

		var deferred = new Deferred();
		deferred.reject(reason);
		return deferred.promise;

		// return new Promise(function (_, reject) {
		// 	reject(reason);
		// });
		// var deferred = new Deferred();
		// deferred.reject(reason);
		// return deferred.promise;
	};

	function promise_mapper (single, inputs, onFulfilled, onRejected) {
		// returns a promise fulfilled when one/all promise(s) in array is/are fulfilled
		// or rejected
		var results = [];
		var length = inputs.length, done = 0;
		var deferred = new Deferred();

		function fulfillmentOperator (value) {
			if (single) {
				deferred.fulfill(value);
			} else {
				results.push(value);
				done++;
				if (done >= length) {
					deferred.fulfill(results);
				}
			}
		}

		function rejectionOperator (reason) {
			// if any of the promises are rejected, reject the promise
			// returned by all
			deferred.reject(reason);
		}

		// each(inputs, function (input) {
		// 	when(input).then(fulfillmentOperator, rejectionOperator);
		// });
		invoke(map(inputs,when), 'then', fulfillmentOperator, rejectionOperator);

		return deferred.promise;
	}

	Promise.all = partial(promise_mapper, false);
	Promise.first = partial(promise_mapper, true);

	// accepts a promise (or pseduo-promise) or a value, and returns a promise
	// essentially a way to handle situations where you may receive either a
	// promise or an immediate value
	function when (promiseOrValue, onFulfilled, onRejected) {
		return Promise.resolve(promiseOrValue).then(onFulfilled, onRejected);
	}

	/* some sugar methods */
	extend(Promise.prototype, {
		or: function (handler) {
			// like a catch clause
			return this.then(void 0, handler);
		},

		regardless: function (handler) {
			// like a finally clause
			var self = this;

			return this.then(function (value) {
				return self.then(handler).resolveWith(value);
			}, function (reason) {
				return self.then(handler).rejectWith(reason);
			});
		},

		resolveWith: function (value) {
			return this.then(function () {
				return value;
			});
		},

		rejectWith: function (reason) {
			return this.then(function () {
				throw reason;
			});
		},

		sleep: function (time) {
			return this.regardless(function () {
				return delay(empty_function, time);
			});
		},

		// if expecting this to be fulfilled with a function
		delay: function (time) {
			var args = slice.call(arguments, 1);
			return this.then(function (fn) {
				return delay.apply(fn, [fn, time].concat(args));
			});
		},

		// forces a promise to be rejected after `time` ms
		timeout: function (time, message) {
			var deferred = new Deferred();
			var timer = setTimeout(function () {
				deferred.reject(new Error(message || 'promise ran out of time'))
			}, time);

			this.then(function (value) {
				clearTimeout(timer);
				deferred.fulfill(value);
			}, function (reason) {
				clearTimeout(timer);
				deferred.reject(reason);
			});

			return deferred.promise;
		},

		// if a promise will be resolved with a function, this calls that function
		// with any number of arguments, returns a new promise fulfilled with the
		// resulting value of calling the function
		call: function (/* args */) {
			return this.delay.apply(this, [0].concat(slice.call(arguments)));
		},

		// joins the current promise with another promise, resulting in
		// a promise that will be resolved with an array of the values
		join: function () {
			return Promise.all([this].concat(slice.call(arguments)));
		},

		done: function () {
			// will throw any unhandled rejection rather than passing it along
			this.then(void 0, function (reason) {
				setTimeout(function () {
					throw reason;
				}, 10);
			});
		}
	});

	function Deferred () {
		var deferred = {};

		deferred.promise = new Promise(function (fulfill, reject) {
			deferred.fulfill = fulfill;
			deferred.reject = reject;
		});

		return deferred;
	}


	// really basic templating
	// <a href="{{url}}">{{text}}</a>
	// { url: 'http://example.com', text: 'Example' }

	var escape_regexp = /\\|'|\r|\n|\t|\u2028|\u2029/g;
	var escape_regexp_map = {
		"'":      "'",
		'\\':     '\\',
		'\r':     'r',
		'\n':     'n',
		'\t':     't',
		'\u2028': 'u2028',
		'\u2029': 'u2029'
	};

	var escape_key = function (key) {
		return key.replace(escape_regexp, function (match) {
			return '\\' + escape_regexp_map[match]; 
		});
	};

	// extremely basic templating function, takes a string, such as:
	// 	var string = '<a href="{{url}}">{{text}}</a> {{ some_other_key }}';
	// and a data object: 
	// 	var data = { text: 'Home', url: 'index.html'};
	// and returns a new string:
	// 	'<a href="index.html">Home</a> ';
	// notice some_other_key was replaced with an empty string
	function template (string, data) {
		// each(data, function (val, key) {
		for (var key in data) {
			var reg = new RegExp('\\{\\{\\s\*' + escape_key(key) + '\\s\*\\}\\}', 'g');
			// if val is a string
			string = string.replace(reg, data[key]);
			// TODO: otherwise?
		}
		// remove any remaining variables from the template before returning
		return string.replace(/\{\{\s*[^\}]+\s*\}\}/g, '');
	}

	function createCookie (name, value, days) {
		var date, expires;
		if (days) {
			date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = '; expires'= + date.toGMTString();
		}
		else expires = '';
		document.cookie = name + '=' + value + expires + '; path=/';
	}

	function readCookie (name) {
		var nameEQ = name + '=';
		var cookies = document.cookie.split(';');
		var i, cookie;
		for (i = 0; i < cookies.length; i++) {
			cookie = cookies[i];
			while (cookie.charAt(0) == ' ') cookie = cookie.substring(1, cookie.length);
			if (cookie.indexOf(nameEQ) == 0) return cookie.substring(nameEQ.length, cookie.length);
		}
		return null;
	}

	function eraseCookie (name) {
		createCookie(name, '', -1);
	}

	function cookie (name, value, days) {
		if (arguments.length == 1) return readCookie(name);
		return createCookie(name, value, days);
	}

 	
 	return {
 	
 		// object utilities
 		create: create,
 		extend: extend,
 		
 		// enumeration functions
 		each: each,
 		map: map,
 		reduce: reduce,
 		filter: filter,
 		invoke: invoke,
 		size: size,
 		pluck: pluck,
 		
 		// function utilities
 		isFunction: isFunction,
 		isArray: isArray,
 		isObject: isObject,
 		bind: bind,
 		partial: partial,
 		wrap: wrap,
 		throttle: throttle,
 		debounce: debounce,
 		delay: delay,
 		defer: defer,
 		when: when,
 		k: function (k) { return k },

 		
 		// Core
 		Base: Base,
 		Constructor: Constructor,
 		Hash: Hash,
 		Namespace: Namespace,
 		Mediator: Mediator,
 		Promise: Promise,
 		Deferred: Deferred,
 		template: template
	};
});
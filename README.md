# README #

This is a personal project, mostly for academic purposes. It's easy to get lazy with all the hip new JavaScript libraries these days, so I decided to write my own to play around with ES5, and to write my own implementations of things I rely on (jQuery, _, etc) all the time.

There's a slick implementation of Promises that conforms to the A+ spec and passes all tests. There's a bunch of enumerable type functions, as well as some optimized DOM manipluation stuff.

The source is kind of a mess, but again it's just a learning/skills-honing project.
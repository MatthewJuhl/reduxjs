describe('redux.extend', function () {
	it('adds properties to an existing object', function () {
		var a = {}, b = {foo: 24};
		redux.extend(a, b);

		expect(a.foo).toBe(24);
	});

	it('overwrites existing properties', function () {
		var a = {foo: 0}, b = {foo: 24};
		redux.extend(a, b);

		expect(a.foo).toBe(24);
	});

	it('works if the source object is null or undefined', function () {
		var a = {foo: 0};
		redux.extend(a, null);
		redux.extend(a, undefined);

		expect(a.foo).toBe(0);
	});

	it("does not add properties from the prototype of the source", function() {
		var a = {};
		var b = function () {};
		b.prototype.foo = 24;
		redux.extend(a,b);

		expect(a.foo).toBe(undefined);
	});

});

describe("redux.create", function() {

	it("creates a new object using an existing object as a prototype", function() {
		var a = { foo: 24 };
		var b = redux.create(a);

		expect(b.foo).toBe(24);
	});

	it("adds specified properties to the newly created object", function() {
		var a = { foo: 24 };
		var b = redux.create({}, a);

		expect(b.foo).toBe(24);
	});

	it ("overwrites properties in the prototype with properties specified in the props object", function(){
		var proto = { foo: 0 };
		var props = { foo: 24 };

		var obj = redux.create(proto, props);

		expect(obj.foo).toBe(24);
	});

	it("creates objects with null prototypes", function() {
		var obj = redux.create(null);
		expect(obj.toString).toBe(undefined);
	});

	it("adds properties to objects created with null prototypes", function () {
		var obj = redux.create(null, { foo: 24 });
		expect(obj.foo).toBe(24);
	});

});

describe("redux.Base", function() {
  
  it("should be an object", function() {
    expect(typeof redux.Base).toBe('object');
  });

  describe("redux.Base#extend", function() {
    it("creates a new object with the properties specified", function() {
      var a = redux.Base.extend({ foo: 24 });
      expect(a.foo).toBe(24);
    });
    it("inherits the extend method from redux.Base", function() {
      var a = redux.Base.extend({ foo: 24 });
      expect(a.extend).toBe(redux.Base.extend);
    });
    it("creates objects that can also be extended", function() {
      var a = redux.Base.extend({ foo: 24 });
      var b = a.extend({});

      expect(b.foo).toBe(24);
      expect(b.extend).toBe(redux.Base.extend);
    });
    it("overwrites prototype members with specified props", function() {
      var a = redux.Base.extend({ foo: 0 });
      var b = a.extend({ foo: 24 });

      expect(b.foo).toBe(24);
    });
  });
});

describe("redux.bind", function() {
  it("binds a function to a given object", function() {
  	 var a = { foo: 24 }, b;
    function f () { return this.foo }
    var g = redux.bind(f, a);
    var test = g();

    expect(test).toBe(24);
  });
});

describe("redux.each", function() {
  it("performs a function using each item of an array", function() {
    var a = [1,2,3];
    var test = [];
    redux.each(a, function (val, key, arr) {
    	test.push(val);
    });
    expect(test[0]).toBe(1);
    expect(test[1]).toBe(2);
    expect(test[2]).toBe(3);
  });

  it("performs a function using each item of an object", function() {
    var a = { foo: 1, bar: 2 };
    var test = [];
    redux.each(a, function (val, key, arr) {
    	test.push(val);
    });
    expect(test[0]).toBe(1);
    expect(test[1]).toBe(2);
  });

  it("passes the current key to the iterator function for each item for an array", function() {
    var a = [1,2,3];
    var keys = [];
    redux.each(a, function (val, key, collection) {
    	keys.push(key);
    });

    expect(keys[0]).toBe(0);
    expect(keys[1]).toBe(1);

  });

  it("passes the current key to the iterator function for each item for an object", function() {
    var a = { foo: 1, bar: 2 };
    var keys = [];
    redux.each(a, function (val, key, collection) {
    	keys.push(key);
    });

    expect(keys[0]).toBe('foo');
    expect(keys[1]).toBe('bar');

  });

  it("passes the enumerated array to the iterator function", function() {
    var a = [24];
    var b;
    redux.each(a, function (val, key, collection) {
    	b = collection;
    });

    expect(b).toBe(a);
  });

  it("passes the enumerated object to the iterator function", function() {
    var a = { foo: 24 };
    var b;
    redux.each(a, function (val, key, collection) {
    	b = collection;
    });

    expect(b).toBe(a);
  });
});

describe("redux.Constructor", function() {
 it("returns a function", function() {
   var f = redux.Constructor(function () {});
   expect(typeof f).toBe('function');
 });

 it("creates a function with a given prototype", function() {
   var f = redux.Constructor(function () {}, { foo: 24 });
   expect(f.prototype.foo).toBe(24);
 });

 it("returns a function that works as a constructor", function() {
   var f = redux.Constructor(function () {});
   var instance = new f();
   var test = instance instanceof f;
   expect(test).toBe(true);
 });

 it("returns a constructor function that creates objects from the specified prototype", function() {
   var f = redux.Constructor(function () {}, { foo: 24 });
   var i = new f();
   expect(i.foo).toBe(24);
 });
});

describe("redux.Hash", function() {
   it("returns an object that references an empty data table", function() {
     var h = new redux.Hash();
     expect(h.data.__proto__).toEqual(undefined);
   });

   it("sets properties specified on the data object", function() {
     var h = new redux.Hash({ foo: 24 });
     expect(h.data.foo).toBe(24);
   });

   describe("redux.Hash#set", function() {
     it('sets a property on the data object of the hash', function () {
     	var h = new redux.Hash();
     	h.set('foo', 24);
     	expect(h.data.foo).toBe(24);
     });
   });

   describe("redux.Hash#get", function() {
     it('returns the value of the property specified', function () {
     	var h = new redux.Hash({ foo: 24 });
     	var foo = h.get('foo');
     	expect(foo).toBe(24);
     });
   });

   describe("redux.Hash#each", function() {
     it("enumerates the data object of the hash", function() {
       var h = new redux.Hash({ foo: 24, bar: 42 });
       var keys = [];
       var vals = [];
       var orig;

       h.each(function (val, key, collection) {
       	keys.push(key);
       	vals.push(val);
       	orig = collection;
       });


       expect(keys[0]).toBe('foo');
       expect(keys[1]).toBe('bar');
       expect(vals[0]).toBe(24);
       expect(vals[1]).toBe(42);
       expect(orig).toBe(h);
     });

     it("passes the hash object as the third argument to the iterator", function() {
       var h = new redux.Hash({ foo: 24, bar: 42 });
       var orig;

       h.each(function (val, key, collection) {
       	orig = collection;
       });

       expect(orig).toBe(h);
     });

     it("calls the iterator in the context of the hash ('this' refers to the hash object)", function() {
       var h = new redux.Hash({ foo: 24, bar: 42 });
       var thisp;

       h.each(function (val, key, collection) {
       	thisp = this;
       });

       expect(thisp).toBe(h);
     });
   });
 });

describe("redux.Namespace", function () {

  var Namespace = redux.Namespace;
  var the_scope = {};

  it('resolves a path within a given scope, creates the namespace with the properties provided, then returns the namespace', function () {

    var ns = Namespace(the_scope, 'foo.bar', { baz: 24 });

    expect(ns.baz).toBe(24);
    expect(the_scope.foo.bar.baz).toBe(24);

  });

  it('uses existing objects when resolving the path, so that previous properties are unmodified', function () {
    var ns = Namespace(the_scope, 'foo.bee', { bop: 24 });
    expect(the_scope.foo.bar.baz).toBe(24);
  });

  it('overwrites existing properties', function () {
    var ns = Namespace(the_scope, 'foo.bar', { baz: 32 });
    expect(ns.baz).toBe(32);
    expect(the_scope.foo.bar.baz).toBe(32);
  });

  it('does not operate on falsey scope objects', function () {
    var null_ns = Namespace(null, 'foo.bar', { baz: 24 });
    var undefined_ns = Namespace(undefined, 'foo.bar', { baz: 24 });
    var false_ns = Namespace(false, 'foo.bar', { baz: 24 });
    var zero_ns = Namespace(0, 'foo.bar', { baz: 24 });

    expect(null_ns).toBe(undefined);
    expect(undefined_ns).toBe(undefined);
    expect(false_ns).toBe(undefined);
    expect(zero_ns).toBe(undefined);
  });

});

describe("redux.Mediator", function() {

  it("creates a new mediator object", function () {
  	var mediator = new redux.Mediator;
  	expect(mediator instanceof redux.Mediator).toBe(true);
  });

  describe("redux.Mediator#options", function() {
    it("sets options for a channel", function() {
      var m = new redux.Mediator;
      m.options('foo', { bar: 24 });
      expect(m._channels.foo.options.bar).toBe(24);
    });

    it("overwrites previously set options", function () {
    	var m = new redux.Mediator;
      m.options('foo', { bar: 0 });
      m.options('foo', { bar: 24 });
      expect(m._channels.foo.options.bar).toBe(24);
    });

    it("extends a channel's options hash rather than replacing it", function () {
    	var m = new redux.Mediator;
      m.options('foo', { bar: 24 });
      m.options('foo', { baz: 32 });
      expect(m._channels.foo.options.bar).toBe(24);
      expect(m._channels.foo.options.baz).toBe(32);
    });
  });

  describe("redux.Mediator#subscribe", function() {
    it("adds a callback to a channel", function() {
      var m = new redux.Mediator;
      function foo () {}
      m.subscribe('bar', foo);
      expect(m._channels.bar.callbacks[0]).toBe(foo);
    });
  });

  describe("redux.Mediator#unsubscribe", function() {
    it("removes a callback from a channels queue", function() {
      var m = new redux.Mediator;
      function foo () {}
      var sub = m.subscribe('bar', foo);
      m.unsubscribe('bar', sub);
      expect(m._channels.bar.callbacks[0]).toBe(undefined);
    });
  });

  describe("redux.Mediator#publish", function() {
    
  	/* this doesn't work because callbacks happen asynchronously */

    it("calls each callback in a channel's callback queue", function() {
      var m = new redux.Mediator;
    	var foo_called, bar_called, baz_called;

    	m.subscribe('foo', function () {
      	foo_called = true;
      });

    	m.subscribe('foo', function () {
      	bar_called = true;
      });

      m.subscribe('baz', function () {
      	baz_called = true;
      });

      runs(function () {
      	m.publish('foo');
      });

      waitsFor(function () {
      	return foo_called;
      }, 'foo and bar callbacks should be called', 250);

      runs(function () {
      	expect(foo_called).toBe(true);
      	expect(bar_called).toBe(true);
      	expect(baz_called).toBe(undefined);
      });
      
    });

    it("should only run a certain number of times if specified with options.times", function() {
        var m = new redux.Mediator;
        var calls = 0, flag;

        m.subscribe('foo', function () {
          calls++;
        });

        m.options('foo', { times: 2 });

        runs(function () {
          m.publish('foo');
          m.publish('foo');
          m.publish('foo'); // shouldn't run
        });

        setTimeout(function () {
          flag = true;
        }, 10);

        waitsFor(function () {
          return flag;
        }, 'foo should be called twice', 1000);

        runs(function () {
          expect(calls).toBe(2);
        });
    });

    it("runs a newly added callback immediately if the event has already been called the allotted number of times", function() {
      var m = new redux.Mediator;
      var calls = 0, flag;

      

      m.options('foo', { times: 2 });

      runs(function () {
        m.publish('foo');
        m.publish('foo');
      });

      m.subscribe('foo', function () {
        flag = true;
      });

      waitsFor(function () {
        return flag;
      }, 'flag should be set to true', 10);

      runs(function () {
        expect(flag).toBe(true);
      });
    });

    it("passes any extra arguments in order to each callback with the channel name as the last argument", function() {
      var m = new redux.Mediator;
      var bar, baz, channel, flag;

      m.subscribe('foo', function (arg1, arg2, arg3) {
        bar = arg1;
        baz = arg2;
        channel = arg3;
      });

      m.publish('foo', 24, 32);

      setTimeout(function () {  
        flag = true;
      }, 10);
      
      waitsFor(function () {
        return flag;
      }, 'flag should be set to true', 1250);

      runs(function () {
        expect(bar).toBe(24);
        expect(baz).toBe(32);
        expect(channel).toBe('foo');
      });
    });
  });

});
(function (global, redux) {
	'use strict';

	var NODE_TYPE_ELEMENT = 1;
	var NODE_TYPE_TEXT = 3;
	var NODE_TYPE_COMMENT = 8;
	var NODE_TYPE_DOCUMENT = 9;
	var NODE_TYPE_FRAGMENT = 11;

	var win = window;
	var doc = win.document;
	var class_list_supported = 'classList' in document.documentElement;

	// detect which event API to use
	var dom2_events_supported = 'addEventListener' in doc;
	var ie_events_supported = !dom2_events_supported && 'attachEvent' in doc;

	var expando_data = [];
	var expando_internal_data = [];
	var expando = '_reduxjs' + (new Date()).getTime();
	var expando_id = 0;

	function get_expando_data (internal, node) {
		if (!node) return;

		var data = expando_data[node[expando]];
		var internal_data = expando_internal_data[node[expando]];

		if (!data) {
			data = redux.create(null);
			internal_data = redux.create(null);
			node[expando] = expando_id++;
			expando_data.push(data);
			expando_internal_data.push(internal_data);
		};

		return internal ? internal_data : data;
	}

	function clear_expando_data (node) {
		if (!node) return;

		if (node[expando]) {
			expando_data[node[expando]] = null;
			expando_internal_data[node[expando]] = null;
		}
	}
	

	// is it a node (element or document fragment)?
	function isNode (thing) {
		return !!(thing && (thing.nodeType === NODE_TYPE_ELEMENT || thing.nodeType === NODE_TYPE_FRAGMENT));
	}

	// calls an iterator on each descendent of a node
	function eachDescendent (node, iterator, context) {
		var descendents = node.getElementsByTagName('*');
		redux.each(descendents, iterator, context || node);
	}

	// cleans up a node before removal
	function scrub (node, scrub_self) {
		// scrub all descendents
		eachDescendent(node, function (descendent) {
			unbindAll(descendent);
			clear_expando_data(descendent);
		});

		// scrub node
		if (scrub_self) {
			unbindAll(node);
			clear_expando_data(node);
		}
	}


	////////////////////////
	// element selection //
	////////////////////////

	// querySelectorAll
	function $ (selector, context) {
		if (!context) var context = document;
		return context.querySelectorAll(selector);
	}

	function $1 (selector, context) {
		if (!context) var context = document;
		return context.querySelector(selector);
	}

	// select elements by tag name
	function $t (tag_name, context) {
		if (!context) var context = document;
		return context.getElementsByTagName(tag_name);
	}

	// get element by ID
	function $i (id, context) {
		if (!context) var context = document;
		return context.getElementById(id);
	}

	// get elements by class name
	function $c (class_name, context) {
		if (!context) var context = document;
		if (!'getElementsByClassName' in context) {
			// Support: IE8
			// getElementsByClassName doesn't exist in IE8, but querySelectorAll does
			return context.querySelectorAll('.' + class_name);
		}
		return context.getElementsByClassName(class_name);
	}



	/////////////////////////
	// class modification //
	/////////////////////////

	
	var hasClass, addClass, removeClass, toggleClass;

	if (class_list_supported) {
		
		hasClass = function (node, class_name) {
			return node.classList.contains(class_name);
		};

		addClass = function (node, class_name) {
			node.classList.add(class_name);
		};

		removeClass = function (node, class_name) {
			node.classList.remove(class_name);
		};

		toggleClass = function (node, class_name) {
			node.classList.toggle(class_name);
		};
	} else {

		hasClass = function (node, class_name) {
			return (' ' + node.className + ' ').indexOf(' ' + class_name + ' ') >= 0;
		};

		addClass = function (node, class_name) {

			if (!hasClass(node, class_name)) {
				node.className += " " + class_name;
			}

		};

		removeClass = function (node, class_name) {

			if (hasClass(node, class_name)) {
				node.className = node.className.replace(new RegExp('\\s*' + class_name + '\\s*','g'), ' ').trim();
			}

		};

		toggleClass = function (node, class_name) {

			if (hasClass(node, class_name)) {
				removeClass(node, class_name);
			} else {
				addClass(node, class_name);
			}

		};

	}



	///////////////////////////////////
	// element content manipulation //
	///////////////////////////////////


	/**
	 * creates a single node or document fragment depending on html
	 * @param  {String} html - a tag name, or a string of html
	 * @return {node}
	 */
	function create (html) {
		if (!/^\w*\</.test(html)) {
			// just a tagname
			return document.createElement(html);
		} else {
			// markup
			
			var div = document.createElement('div');
			div.innerHTML = html;
			div.style.display = 'none';
			append(div);
			if (div.children.length > 1) {
				var fragment = doc.createDocumentFragment();
				var child;
				while (child = div.firstChild) {
					fragment.appendChild(child);
				}
				div.parentNode.removeChild(div);
				div = null;
				return fragment;
			} else {
				// single element
				var node = div.removeChild(div.firstChild);
				div.parentNode.removeChild(div);
				div = null;
				return node;
			}
		}
	}

	// remove all contents of node
	function empty (node) {
		// scrub descendent nodes
		scrub(node, false);
		node.innerHTML = '';
		node.innerText = '';
	}

	// remove a node from the DOM
	function detach (node, parentNode) {
		if (parentNode) {
			return parentNode.removeChild(node);
		} else {
			return node.parentNode.removeChild(node);
		}
	}	

	// remove a node from the DOM
	function remove (node, parentNode) {
		scrub(node, true);
		node.innerHTML = '';
		return detach(node, parentNode);
	}

	// insert a node after the referenceNode
	function insertBefore (insertedNode, referenceNode) {
		if (typeof insertedNode === 'string') {
			// allow html string
			insertedNode = create(insertedNode);
		}
		if (arguments.length < 2) var referenceNode = doc.body ? doc.body : doc;
		var parentNode = referenceNode.parentNode;
		return parentNode.insertBefore(insertedNode, referenceNode);
	}

	function insertAfter (insertedNode, referenceNode) {
		if (typeof insertedNode === 'string') {
			// allow html string
			insertedNode = create(insertedNode);
		}
		if (arguments.length < 2) var referenceNode = doc.body ? doc.body : doc;
		var parentNode = referenceNode.parentNode;
		return parentNode.insertBefore(insertedNode, referenceNode.nextSibling);
	}

	function append (node, container) {
		if (typeof node === 'string') {
			// allow html string
			node = create(node);
		}
		if (arguments.length < 2) var container = doc.body ? doc.body : doc;
		container.appendChild(node);
	}

	function prepend (node, container) {
		if (typeof node === 'string') {
			// allow html string
			node = create(node);
		}
		if (arguments.length < 2) var container = doc.body ? doc.body : doc;
		insertBefore(node, container.firstChild);
	}

	function html (node, html) {
		if (arguments.length === 1) {
			return node.innerHTML;
		}
		try {
			node.innerHTML = html;
		} catch (e) {
			// TODO: use DOM manipulation if the above fails
		}
		
	}

	function appendHtml (node, html) {
		if (node.insertAdjacentHTML) {
			node.insertAdjacentHTML('beforeend', html)
		} else {
			// node.innerHTML += html;
			var current = html(node);
			html(node, current + html);
		}
	}
	 
	function prependHtml (node, html) {
		if (node.insertAdjacentHTML) {
			node.insertAdjacentHTML('afterbegin', html);
		} else {
			var current = html(node);
			html(node, html + current);
			// node.innerHTML = html + node.innerHTML;
		}
	}

	function replaceWith (node, replacement) {
		if (arguments.length < 2) var replacement = doc.body ? doc.body : doc;
		if (typeof replacement === 'string') replacement = create(replacement);

		return node.parentNode.replaceChild(replacement, node);
	}

	function wrap (node, wrapper) {
		if (typeof wrapper === 'string') wrapper = create(wrapper);

		var innerNode = wrapper;
		while (innerNode.firstChild && innerNode.firstChild.nodeType === NODE_TYPE_ELEMENT) {
			innerNode = innerNode.firstChild;
		}
		replaceWith(node, wrapper);
		append(node, innerNode);
	}

	function wrapInner (node, wrapper) {
		// move all of node's children to a fragment
		var node_contents = document.createDocumentFragment();
		while (node.firstChild) {
			append(node.firstChild, node_contents);
		}

		if (typeof wrapper === 'string') {
			html(node, wrapper);
		} else {
			append(wrapper, node);
		}

		var innerNode = node.firstChild;
		while (innerNode.firstChild && innerNode.firstChild.nodeType === NODE_TYPE_ELEMENT) {
			innerNode = innerNode.firstChild;
		}

		append(node_contents, innerNode);
	}

	/////////////////////////////////////////////
	// element attribute and property helpers //
	/////////////////////////////////////////////

	// set or get data-{key} attribute of node
	function dataAttr (node, key, value) {
		key = 'data-' + key;
		if (arguments.length > 2) {
			node.setAttribute(key, value);
		} else {
			return node.getAttribute(key);
		}
	}

	/* there is no style manipulation function because it's bad! use classes! */

	//////////////////////
	// event listeners //
	//////////////////////

	var bind, unbind, trigger;
	var event_types = {};
	event_types.click = event_types.mousedown = event_types.mouseup = event_types.mousemove = 'MouseEvents';

	// TODO: add a method for dispatching events

	// patch preventDefault/stopPropagation
	if (!Event.prototype.preventDefault) {
		Event.prototype.preventDefault = function() {
			this.returnValue = false;
		};
	}
	 
	if (!Event.prototype.stopPropagation) {
		Event.prototype.stopPropagation = function() {
			this.cancelBubble = true;
		};
	}

	if (dom2_events_supported) {
		// addEventListener
		bind = function (node, eventName, callback) {
			node.addEventListener(eventName, callback, false);

			// save a copy of all bound events for batch removal
			var data = get_expando_data(true, node);
			var ns = redux.Namespace(data, 'attached_events.' + eventName);
			ns[callback] = callback;
		};

		unbind = function (node, eventName, callback) {
			node.removeEventListener(eventName, callback, false);

			// delete saved copy of bound event
			var data = get_expando_data(true, node);
			var ns = redux.Namespace(data, 'attached_events.' + eventName);
			ns[callback] = null;
			delete ns[callback];
		};

		trigger = function (node, eventName, eventProperties) {
			var e = document.createEvent(event_types[eventName] || 'Events');
			e.initEvent(eventName, true, true);
			redux.extend(e, eventProperties);
			node.dispatchEvent(e);
		};

	} else if (ie_events_supported) {

		// attachEvent
		bind = function (node, eventName, callback) {
			// build a wrapper around the callback to be bound
			var bound = function (e) {
				// patch event object
				e.target = e.srcElement;
				e.currentTarget = node;
				
				// use handleEvent if it's there
				if (callback.handleEvent) {
					callback.handleEvent(e);
				} else {
					callback.call(node, e);
				}
			};
			// add the listener
			node.attachEvent('on' + eventName, bound);
			// save the bound function for removal
			// first, get the node's internal data
			var data = get_expando_data(true, node);
			// create namespace for this event if it doesn't exist
			var ns = redux.Namespace(data, 'attached_events.' + eventName);
			// save the bound callback using the callback as the key
			ns[callback] = bound;
		}

		unbind = function (node, eventName, callback) {
			// retrieve the bound callback from the node's internal data
			var data = get_expando_data(true, node);
			var ns = redux.Namespace(data, 'attached_events.' + eventName);
			var bound = ns[callback];
			node.detachEvent('on' + eventName, bound);
			ns[callback] = null;
			delete ns[callback];
		};

		trigger = function (node, eventName, eventProperties) {
			var e = doc.createEventObject();
			redux.extend(e, eventProperties);
			node.fireEvent('on' + eventName, e);
		};

	} else {
		// TODO: last resort, use node[onEventName] style
		bind = function (node, eventName, callback) {}
		unbind = function (node, eventName, callback) {};
	}

	// removes all attached event listeners
	function unbindAll (node) {
		var data = get_expando_data(true, node);
		var ns = redux.Namespace(data, 'attached_events');
		for (var eventName in ns) {
			var callbacks = ns[eventName];
			for (var key in callbacks) {
				redux.dom.unbind(node, eventName, ie_events_supported ? key : callbacks[key]);
				callbacks[key] = null;
				delete callbacks[key];

			}
		}
	}


	function delegate (node, eventName, selector, callback) {
		var interceptor = function (e) {
			// the node the event was triggered on originally
			var originNode = e.srcElement || e.originalTarget;
			// normalize
			if (!'srcElement' in e) e.srcElement = originNode;
			var selectorMatches = $(selector, node);

			redux.each(selectorMatches, function (matchedNode) {
				if (matchedNode === originNode) {
					callback.call(matchedNode, e);
				}
			});

		};

		bind(node, eventName, interceptor);

		// save the interceptor for undelegation
		var data = get_expando_data(true, node);
		// create namespace for this event if it doesn't exist
		var ns = redux.Namespace(data, 'delegated_events.' + eventName);
		if (!ns[selector]) ns[selector] = {};
		// save the bound callback using the callback as the key
		ns[selector][callback] = interceptor;
	}

	function undelegate (node, eventName, selector, callback) {
		// retrieve the interceptor from internal expando data
		var data = get_expando_data(true, node);
		// create namespace for this event if it doesn't exist
		var ns = redux.Namespace(data, 'delegated_events.' + eventName);
		if (!ns[selector]) ns[selector] = {};
		// save the bound callback using the callback as the key
		var interceptor = ns[selector][callback];

		unbind(node, eventName, interceptor);
	}



	///////////////////
	// form helpers //
	///////////////////

	// checks whether a checkbox/radio is checked
	function isSelected (node) {
		if (typeof node === 'string') {
			// accept element ID
			node = $i(node);
		}
		return node.checked || node.selected
	}

	// gets the selected radio/checkboxes within context
	// returns a string (if radios or single checkbox) or an array (if checkboxes)
	function getSelected (selector, context) {
		// make sure context is a single node
		if (typeof context === 'string') context = $1(context);

		// intelligent guess based on the context
		if (!selector) selector = context.nodeName === 'SELECT' ? 'option' : '*';

		var inputs = /\W/.test(selector) ? $(selector, context) : $t(selector, context);
		var checked = redux.filter(inputs, function (node) {
			return node.checked || node.selected;
		});

		return checked;
	}

	function getSelectedValues (selector, context) {
		var selected = getSelected(selector, context);
		return redux.map(selected, function (node) {
			return node.value;
		});
	}



	///////////////////////
	// element data api //
	///////////////////////

	var get_public_expando_data = redux.partial(get_expando_data, false);

	function data (node, key, val) {
		var data = get_public_expando_data(node);

		switch (arguments.length) {
			case 0: 
				return;
			case 1:
				return data;
			case 2:
				return data[key];
			default: 
				data[key] = val;
		}
	}

 	/*
	if (typeof exports !== 'undefined') {
		// Node/CommonJS
		exports.redux = factory();
	} else if (typeof define === 'function' && define.amd) {
		// AMD/RequireJS
		define('redux', [], factory);
	} else {
		// Browser global
		global.redux = factory();
	}
	*/

	////////////////////////
	// export public api //
	////////////////////////

	redux.dom = {

		isNode: isNode,
		eachDescendent: eachDescendent,

		// element selection
		$: $,
		$1: $1,
		$t: $t,
		$i: $i,
		$c: $c,

		// class modification
		addClass: addClass,
		removeClass: removeClass,
		hasClass: hasClass,
		toggleClass: toggleClass,

		// element content manipulation
		empty: empty,
		remove: remove,
		create: create,
		append: append,
		prepend: prepend,
		insertBefore: insertBefore,
		insertAfter: insertAfter,
		html: html,
		appendHtml: appendHtml,
		prependHtml: prependHtml,
		replaceWith: replaceWith,
		wrap: wrap,
		wrapInner: wrapInner,
		dataAttr: dataAttr,

		// form helpers
		isChecked: isSelected, isSelected: isSelected, // aliases
		getChecked: getSelected, getSelected: getSelected, // aliases
		getSelectedValues: getSelectedValues,

		// events
		bind: bind,
		unbind: unbind,
		trigger: trigger,
		unbindAll: unbindAll,
		delegate: delegate,
		undelegate: undelegate,

		// data
		data: data,

		
		// just a comma helper
		__remove_me__: 0
	}
 
})(this, this.redux);
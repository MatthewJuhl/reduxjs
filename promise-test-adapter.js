var rdx = require('./lib/rr');

// creates a promise that is already fulfilled with value.
exports.fulfilled = function (value) {
	var deferred = rdx.Deferred();
	deferred.fulfill(value);
	return deferred.promise;
}

// creates a promise that is already rejected with reason
exports.rejected = function (reason) {
	var deferred = new rdx.Deferred;
	deferred.reject(reason);
	return deferred.promise;
}

exports.pending = function () {
	return new rdx.Deferred;
}

